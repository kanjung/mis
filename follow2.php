<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Followup</title>
<style>
    td.ng {
        color: red;
        text-align: left;
    }

    button {
        background: none;
    }
</style>
</head>
<link rel="stylesheet" href="css/bootstrap3.4.1.min.css">
<body>
<?php include('template/leftbar.php'); ?>
<?php include('template/topbar.php'); ?>
<div class="container" style="padding-top: 5rem ; ">
    <h3>FOLLOW UP:</h3>
    <ul class="nav nav-tabs">
        <li ><a href="followup.php">FOLLOW UP</a></li>
        <li class="active"><a href="follow2.php">Waiting For Approvel</a></li>
    </ul>
</div>

<div class="container text-left">
    <div class="col-xl-16">
        <table class="table table-centered mb-0">
            <form id="postform" name="form1" class="form" action="" method="post">
                <thead>

                    <tr>
                        <th>NO</th>
                        <th>Client</th>
                        <th>Subject</th>
                        <th>Assigned Date</th>
                        <th>Due Date</th>
                        <th>Assigned TO</th>
                        <th>APPROVED</th>
                    </tr>
                </thead>
                <?php
                $query = "SELECT * FROM task INNER JOIN 
                approved on task.task_id = approved.task_id  where approved.member_id = '" .$us. "' AND task.status = '3' or task.status = '5' group by approved.task_id desc ";
                $query_run = mysqli_query($conn, $query);
                $m = 0;
                while ($row = mysqli_fetch_array($query_run, MYSQLI_ASSOC)) {
                    $datenow = date('Y-m-d');  //วันที่ปัจจุบัน
                ?>
                    <tbody>
                        <?php
                        $m++;

                        $date =  explode(' ',$row['due_date']);
                        $start_time  = $date[0];
                        $datee = explode('-', $start_time);
                        $year =  $datee[0];
                        $month = $datee[1] ;
                        $day = $datee[2];    
                        $enddate = $day.'/'.$month.'/'.$year;
                        
                        $date1 =  explode(' ', $row['assigned_date']);
                        $start_time1  = $date1[0];
                        $datee1 = explode('-', $start_time1);
                        $year1 =  $datee1[0];
                        $month1 = $datee1[1] ;
                        $day1 = $datee1[2];  
                        $startdate = $day1.'/'.$month1.'/'.$year1; 
    
                        ?>
                <tr>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $m ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$m; </td> ';
                            }
                            echo $m;
                            ?>
  
                 <?php  $sql3 = "SELECT customer_name from customer where cus_id = '".$row['client']."' ";
                $query3 = mysqli_query($conn,$sql3);
                while($rowc = mysqli_fetch_array($query3,MYSQLI_ASSOC)){ ?>     
            
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $client ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$client; </td> ';
                            }
                            echo $rowc['customer_name']; 
                        ?> 
                           <?php } ?>
                         
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $subject ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$subject; </td> ';
                            }
                            echo $row['subject'];
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $startdate ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$startdate; </td> ';
                            }
                            echo $startdate;
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $enddate ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$enddate; </td> ';
                            }
                            echo $enddate;
                            ?>

                        <?php
                            $strSql = "SELECT * FROM assigned_task WHERE  task_id  = '" . $row['task_id'] . "'  ";
                            $query3 = mysqli_query($conn, $strSql);
                            $num = mysqli_num_rows($query3);
                            $resultArray = array();
                            for ($i = 0; $i < $num; $i++) {
                                $result3 = mysqli_fetch_array($query3);
                                array_push($resultArray, $result3);
                            }
                            ?>
                        <?php
                            foreach ($resultArray as $result3) {
                                if ($row['due_date'] >= $datenow) {
                                    echo '<td class="row" $result3["assigned"];  </td>';
                                }
                                if ($row['due_date'] < $datenow) {
                                    echo '<td class="ng row".$result3["assigned"]; </td>';
                                }
                                echo $result3["assigned"];
                            ?>
                                </td>
                            <?php } ?>
                           
                            <td> <a href="approved.php?task_id=<?php echo $row['task_id']; ?>" type="button" class="btn btn-dark" >Approved </a></td>
                        <?php } ?>
                        </tr>
    </div>
</div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
    
    $(document).on('click', '#noti', function() {
        var uid = $(this).attr("data-id");
      //  var status = confirm("Are you Send Alert");
       // if (status) {
            $.ajax({
                url: "add_noti.php",
                method: "post",
                data: {
                    id: uid
                },
                success: function(data) {
                    location.reload();
                }
            })
       // }
    });

    $(document).on('click', '#notiapp', function() {
        var uid = $(this).attr("data-id");
        
        $.ajax({
            url: "add_noti2.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                location.reload();
            }
        })
    });

</script>
</body>
</html>