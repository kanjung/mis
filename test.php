<?php
session_start();
if (isset($_POST["email_address"])) 
{
    require_once("loginmember.php");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>MIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" id="light-style" />-->
        <link href="css/login-tlo.css" rel="stylesheet" type="text/css" id="light-style" />
    </head>
    <body class="authentication-bg pb-0">
        <div class="auth-fluid">
            <div class="auth-fluid-form-box">
                <div class="align-items-center d-flex h-100">
                    <div class="card-body">
                        <p class="text-muted mb-4 text-right"><img src="assets/images/Tlogical_Logo_Whit.png" height='50px'></p>
                    </div>
                </div>
            </div>
            <div class="auth-fluid-right text-center">
            <div class="align-items-center d-flex h-100">
                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <input class="form-control" type="email" id="email_address" name="email_address" placeholder="Email Address" onkeyup="onaction();">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" id="password" name="password" placeholder="Password" onkeyup="onaction();">
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-dark btn-rounded" type="button" id="btnlogin"  disabled></i>Check it out and let’s rock it!</button>
                            </div>
                        </form>
                        <footer class="footer footer-alt">
                            <p class="text-muted"><a href="https://www.tlogical.com/2020/" class="text-muted ml-1">Copyright © 2020 T.logical Resolution. All rights reserved</a></p>
                        </footer>

                    </div>
                </div> 

            </div>
        </div>
        <script src="js/vendor.min.js"></script>
        <script src="js/app.min.js"></script>
        <script src="js/jquery.filter_input.js"></script>
        <script src="js/swal.js"></script>
        <script>
    $(document).ready(function() {
        $('#btnlogin').click(function() {
            if ($("#email_address").val() == "" && $('#password').val() == "") {
                $("#email_address").css("border", "1px solid red");
                $("#password").css("border", "1px solid red");
                return false;
            }
            if ($("#email_address").val() == "") {
                $("#email_address").css("border", "1px solid red");
                return false;
            }
            if ($('#password').val() == "") {
                $("#password").css("border", "1px solid red");
                return false;
            }
            $("#fromlogin").submit();
            return false;
        });
    });

    $(document).ready(function() {
        $(document).bind('keypress', function(e) {
            if (e.keyCode == 13) {
                $('#fromlogin').submit();
                return false;
            }
        });
    });

    $(document).ready(function() {
        $('#email_address').filter_input({
            regex: '[a-zA-Z0-9@._-]'
        });
    });

    /*
            function check() {
                obj = document.fromlogin
                if (obj.email_address.value != "" && obj.password.value != "") {
                    obj.Submit.disabled = false
                }
            }

    */

    function onaction() {
        if (document.getElementById('email_address').value != "" && document.getElementById('password').value != "") {
            document.getElementById('btnlogin').disabled = false;
        } else if (document.getElementById('email_address').value != "" || document.getElementById('password').value !=
            "") {
            document.getElementById('btnlogin').disabled = true;
        }
    }
    </script>
    </body>
</html>