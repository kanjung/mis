<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Viewproject</title>
<?php
$perpage = 10;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$start = ($page - 1) * $perpage;
$m = 0;

$query = "SELECT *  FROM projects GROUP BY project_id DESC limit {$start},{$perpage}  ";
$query_run = mysqli_query($conn, $query);
?>
</head>
<body>
    <?php   include('template/leftbar.php');
            include('template/topbar.php'); ?>

<div class="container text-left">
    <form action="">
        <div class="col-xl-12 text-right" style="padding-top: 5rem ;">
            <a href="project.php" type="button" class="btn btn-dark">AddProject</a> </div>
        <table class="table table-centered mb-0 ">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Customer</th>
                    <th>Project Name</th>
                    <th>assigned</th>
                    <th>Project Manager</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($page > 1) {
                    $m = ($page * $perpage) - 10;
                }

                while ($row = mysqli_fetch_array($query_run, MYSQLI_ASSOC)) {
                    $m++;
                ?>
            <tbody>
                <tr>
                    <td> <?php echo $m; ?> </td>
                    <td> <?php $sql3 = "SELECT customer_name from customer where cus_id = '" . $row['customer'] . "' ";
                            $query3 = mysqli_query($conn, $sql3);
                            while ($rowc = mysqli_fetch_array($query3, MYSQLI_ASSOC)) { ?>

                            <?php echo $rowc['customer_name']; ?> </td>
                <?php } ?>
                <td> <?php echo $row['project_name']; ?> </td>

                <?php $sql1 = "SELECT assigned from assigned where project_id = '" . $row["project_id"] . "' ";
                    $query1 = mysqli_query($conn, $sql1);
                    while ($rowb = mysqli_fetch_array($query1, MYSQLI_ASSOC)) { ?>

                    <td class="row cols-3"><?php echo $rowb['assigned']; ?></td>
                <?php } ?>

                <td> <?php echo $row['project_manager']; ?> </td>
                <td>
                    <?php
                    if ($row['member_id'] == $us) {
                        $project_id = $row["project_id"];
                        echo '<a href="edit_project.php?project_id=' . $project_id . ' " type="button" class="btn btn-dark" id="edit_data">Edit</a>';
                    } else {
                        echo '<button disabled="disabled" type="button" class="btn btn-dark" id="edit_data">Edit</button>';
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($row['member_id'] == $us) {
                        echo '<button data-id= "' . $project_id . '" type="button" id="delete_data" class="btn btn-dark">Delete</button>';
                    } else {
                        echo '<button disabled="disabled" type="button" class="btn btn-dark" id="delete_data">Delete</button>';
                    }
                    ?>
                </td>
                </tr>
            <?php } ?>
    </form>
    </tbody>
</div>


<tr>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>

    <td>
        <?php
        $sql2 = "SELECT * from projects ";
        $query2 = mysqli_query($conn, $sql2);
        $total_record = mysqli_num_rows($query2);
        $total_page = ceil($total_record / $perpage);

        if ($total_record > 10) {
        ?>
            <nav aria-label="Page navigation exaple mt-5">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="view_project.php?page=1">Previous</a>
                    </li>
                    <?php for ($i = 1; $i <= $total_page; $i++) { ?>
                        <li class="page-item <?php if ($page == $i) {
                                                    echo 'active';
                                                } ?>">
                            <a class="page-link" href="view_project.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li>
                    <?php } ?>
                    <li class="page-item">
                        <a class="page-link" href="view_project.php?page=<?php echo $total_page; ?>">
                            Next
                        </a>
                    </li>
                </ul>
            </nav>
        <?php  }   ?>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
        <div class="text-right">
            <a href="create.php" type="button" class="btn btn-dark">Back</a>
        </div>
    </td>

</tr>
<!----------------------------------------------------------------------------------------------------->
<script src="js/swal.js"></script>
<script>
    $(document).on('click', '#delete_data', function() {
        var uid = $(this).attr("data-id");
        //  var status = confirm("Are you Delete");
        //  if (status) {
        $.ajax({
            url: "checkdelete.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                if (data == 1) {
                    swal.fire({
                        icon: 'error',
                        text: 'ไม่สามารถลบข้อมูลได้เนื่องจาก Project นี้มี My Task ที่เกี่ยวข้องอยู่'
                    })
                }
                if (data == 0) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "คุณต้องการลบ Project ใช่หรือไม่",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire(
                                'Deleted!',
                                'ลบข้อมูลสำเร็จ.',
                                'success'
                            )

                            $.ajax({
                                url: "delete_project.php",
                                method: "post",
                                data: {
                                    id: uid
                                },
                                success: function(data) {
                                    location.reload()
                                }
                            })
                        }
                    });
                }
            }
        });
    });
</script>
</body>
</html>