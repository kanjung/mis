<style>
* {box-sizing: border-box;}

body { 
  margin: 5;
  font-family: Arial, Helvetica, sans-serif;
}

.header {
  overflow: hidden;
  padding: 20px 10px;
}


body .modal-open 
{
  padding-right: 0px !important;
  overflow-y: auto;
  }

.header a {
  float: center;
  color: black;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  font-size: 18px; 
  line-height: 45px;
  border-radius: 4px;
}

.header a.logo {
  font-size: 25px;
  font-weight: bold;
}

.header a:hover {
  color: black;
}

.header a.active {
  color: white;
}

.header-right {
  float: right;
}

@media screen and (max-width: 500px) {
  .header a {
    float: none;
    display: block;
    text-align: right;
  }
  
  .header-right {
    float: left;
  }
}
#show_notification p
{
    margin-right:20px;
    margin-top:20px;

  }
 
  #notifications 
         {
         display:none;
         width:300px;
         top:50px;
         right:5px;
         background:#FFF;
         border:solid 1.5px rgba(100, 100, 100, .20);
         z-index: 1;
         overflow: scroll; /* showing scrollbars */
         border:solid 1.5px rgba(100, 100, 100, .20);
        z-index: 1;
        position:absolute;


          }
          #notifications_button 
          {
          margin-left: 3px;
          width: 32px;
          height: 32px;
          line-height:30px;
          border-radius:50%;
          margin-right: 5px;
          -moz-border-radius:50%; 
          -webkit-border-radius:50%;
          cursor:pointer;
         }
  #notifications_counter 
        {
         display:block;
         position:absolute;
         background:#E1141E;
         color:#FFF;
         font-size:12px;
         font-weight:normal;
         padding:1px 3px;
         border-radius:2px;
         -moz-border-radius:2px; 
         -webkit-border-radius:2px;
         z-index:1;
         }

         #notifications_button .notifications_bell
         {
         background-image: url("assets/images/inbox.svg");
         width: 35px;
         height: 35px;
         }
         
         #notification_box
         {
         margin-bottom:10px;
         }
</style>
<div class="header-right" >
<button class="btn btn-dark"  id="logout"> 
  <i class="fas fa-sign-out-alt"></i>
    <span>Logout</span>
</button>
   <!-- LOGO -->
<a href="home.php" class="topnav-logo">
    <span class="topnav-logo-sm">
        <img src="assets/images/Tlogical_icon_Navy.png" style="width:32px;">
</a>
        </div>  
        <div class="header-right" > 
<div id="notification_box ">
<?php 
  $res_message=mysqli_query($conn,"SELECT * FROM notification WHERE notification.status='0' 
  and notification.to_id='".$us."' group by no_id desc ");
          $unread_count=mysqli_num_rows($res_message); ?>
           <ll id="notifications_container">
               <div id="notifications_counter"> <?php echo $unread_count?> </div>
               <div id="notifications_button">
                  <div class="notifications_bell white"></div>
                  </div>
        <div id="notifications">
          <div style="height:300px;" id="show_notification">
          <?php if($unread_count>0){
            while($row_message=mysqli_fetch_assoc($res_message)) 
            { 
              $task_id = $row_message['task_id'];
              $sql = "SELECT email_address from member where member_id = '".$row_message['from_id']."' ";
              $query = mysqli_query($conn,$sql);
              while($row = mysqli_fetch_array($query,MYSQLI_ASSOC))
            {
              $sql5 = "SELECT project_id from task where task_id = '".$row_message['task_id']."' ";
              $query5 = mysqli_query($conn,$sql5);
              while($row5 = mysqli_fetch_array($query5,MYSQLI_ASSOC)){
            ?>
              <div class="row container">  
                <a id="notibox" data-id="<?php echo $row_message['no_id'];?>" href="detail_project.php?project_id=<?php echo $row5["project_id"]; ?>" > <strong><?php echo $row['email_address']?>
                  </strong> Message <?php echo $row_message['message']?><input type="hidden" id="no" value="<?php echo $row_message['no_id']?>">   
                </a>
              </div>
  <?php } } } }?>
          </div>            
        </div>
</div> 
</div>  
  <?php
            $sql9 = "SELECT * from task where status = '1' ";
            $query9 = mysqli_query($conn,$sql9);
            while($row5 = mysqli_fetch_array($query9,MYSQLI_ASSOC)){
                $datenow = date('Y-m-d');  
              if ($row5['due_date'] < $datenow && $row5['status'] =='1'){
                 $query7 = "UPDATE task SET
                 status = '2'
                 where task_id = '" . $row5['task_id'] . "'   ";
                 $query9 = mysqli_query($conn,$query7);
            ?>   
              <input type="text" id="wait" name="wait" value="<?php echo $row5['task_id']?>">
  <?php } }  ?>
</body>
</html>
<script src="js/swal.js"></script>
<script src="js/jquery-3.5.1.min.js"></script>

<script>

          $(document).ready(function () {

          $('#notifications_button').click(function () {
                jQuery.ajax({
                    success:function(){
                        $('#notifications').fadeToggle('fast', 'linear');
                        $('#notifications_counter').fadeOut('slow');
                }
              })
              return false;
        });
      });

      $(document).on('click', '#notibox', function() {
            var uid = $(this).attr("data-id");
                jQuery.ajax({
                    method: "post",
                    url:'update_message_status.php',
                    data: {
                        id: uid
                    },
                    success:function(){
                      $('#notifications').fadeToggle('fast', 'linear');
                }
              })
            });
        //  $(document).click(function () {
         //       $('#notifications').hide(); 
         //   });
        // });

        $(document).ready(function () {
          $('#logout').click(function () {
            Swal.fire({
                        text: "ต้องการออกจากระบบใช่หรือไม่!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Logout'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: "logout.php",
                                method: "post",
                                success: function(data) {
                                  location.reload();
                                }    
                    })
                  }
                  });
                }); 
              });
                  
          if ($("#wait").val() != "" ){       
            $.ajax({
            url: "add_notiwait.php",
            method: "post",
            data: {
              wait: $("#wait").val()
            },
             
        });
      } 
        </script>