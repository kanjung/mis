<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap3.4.1.min.css">

    <title>Followup</title>
<style>
    td.ng {
        color: red;
        text-align: left;
    }

    button {
        background: none;
    }
</style>
</head>
<body style=" font-size: 1.5rem;">
<?php include('template/leftbar.php'); 
      include('template/topbar.php'); ?>
<div class="container" style="padding-top: 5rem ;">
    <h3>FOLLOW UP:</h3>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#">FOLLOW UP</a></li>
        <li><a href="follow2.php">Waiting For Approvel</a></li>
    </ul>
</div>

<div class="container text-left">
    <div class="col-xl-16">
        <table class="table table-centered mb-0">
            <form id="postform" name="form1" class="form" action="" method="post">
                <thead>

                    <tr>
                        <th>NO</th>
                        <th>Client</th>
                        <th>Subject</th>
                        <th>Assigned Date</th>
                        <th>Due Date</th>
                        <th>Assigned TO</th>
                        <th>Alert</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <?php
                $perpage = 10;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 1;
                }
                $start = ($page - 1) * $perpage;

                $query = "SELECT * FROM task INNER JOIN 
                approved on task.task_id = approved.task_id  where approved.member_id = '" .$us. "' group by approved.task_id desc limit {$start},{$perpage} ";
                $query_run = mysqli_query($conn, $query);
                $m = 0;
                while ($row = mysqli_fetch_array($query_run, MYSQLI_ASSOC)) {
                    $datenow = date('Y-m-d');  //วันที่ปัจจุบัน
                ?>
                    <tbody>
                        <?php
                           if ($page > 1) {
                            $m = ($page * $perpage) - 10;
                        }
                        $m++;
                        $date =  explode(' ',$row['due_date']);
                        $start_time  = $date[0];
                        $datee = explode('-', $start_time);
                        $year =  $datee[0];
                        $month = $datee[1] ;
                        $day = $datee[2];    
                        $enddate = $day.'/'.$month.'/'.$year;
                        
                        $date1 =  explode(' ', $row['assigned_date']);
                        $start_time1  = $date1[0];
                        $datee1 = explode('-', $start_time1);
                        $year1 =  $datee1[0];
                        $month1 = $datee1[1] ;
                        $day1 = $datee1[2];  
                        $startdate = $day1.'/'.$month1.'/'.$year1; 
    
                        ?>
                <tr>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $m ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$m; </td> ';
                            }
                            echo $m;
                            ?>
  
                 <?php  $sql3 = "SELECT customer_name from customer where cus_id = '".$row['client']."' ";
                $query3 = mysqli_query($conn,$sql3);
                while($rowc = mysqli_fetch_array($query3,MYSQLI_ASSOC)){ ?>     
            
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $client ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$client; </td> ';
                            }
                            echo $rowc['customer_name']; 
                        ?> 
                           <?php } ?>
                         
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $subject ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$subject; </td> ';
                            }
                            echo $row['subject'];
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $startdate ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$startdate; </td> ';
                            }
                            echo $startdate;
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $enddate ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$enddate; </td> ';
                            }
                            echo $enddate;
                            ?>

                        <?php
                            $strSql = "SELECT * FROM assigned_task WHERE  task_id  = '" . $row['task_id'] . "'  ";
                            $query3 = mysqli_query($conn, $strSql);
                            $num = mysqli_num_rows($query3);
                            $resultArray = array();
                            for ($i = 0; $i < $num; $i++) {
                                $result3 = mysqli_fetch_array($query3);
                                array_push($resultArray, $result3);
                            }
                            foreach ($resultArray as $result3) {
                                if ($row['due_date'] >= $datenow) {
                                    echo '<td class="row" $result3["assigned"];  </td>';
                                }
                                if ($row['due_date'] < $datenow) {
                                    echo '<td class="ng row".$result3["assigned"]; </td>';
                                }
                                echo $result3["assigned"];
                            ?>
                                </td>
                            <?php } ?>
                            <td>
                        <?php
                                $query1 = "SELECT * from notification WHERE  task_id = '".$row['task_id']."' AND  to_id = '".$us."'  ORDER BY no_id DESC LIMIT 1";
                                $run = mysqli_query($conn,$query1);
                                $fetch = mysqli_fetch_array($run,MYSQLI_ASSOC);
                                $task_id = $row['task_id'];
                                if(isset($fetch)!=''){
                                $clickdate = $fetch['clickdate'];
                                $enddate = $fetch['enddate'];
                            }
                                $pathx = "assets/images/time.png";
      
                                    if ($datenow >= $enddate) {
                                        $pathx = "assets/images/time.png";
                                        echo '<button  id="notiapp" type="button" name="notiapp" data-id="' . $task_id . '"> <img src="' . $pathx . '" width="30pix" width="30pix"> </button>';
                                    } else {
                                        $pathx = "assets/images/time.png";
                                        echo '<button disabled="disabled"><img src="' . $pathx . '" width="30pix";></button>';
                                    }
                            
                            ?>
                            </td>
                       <?php     
                            if ( $row["status"] == '1' &&  $row['due_date']>=$datenow)
                            {   
                               $pathx = "assets/images/power.png";
                            echo ' <td> On <img src="'.$pathx.'" width="28"pix" ></td>';  
                            }
                            elseif ($row["status"] == '2' && $row['due_date']<$datenow) 
                            {   
                               $pathx = "assets/images/alert1.jpg";
                            echo ' <td class="ng">Overdue<img src="'.$pathx.'" width="30pix"; > </td> ';
                            } 
                            if  ($row["status"]== "3" &&  $row['due_date']<$datenow ) {
                                $pathx = "assets/images/loading.png";
                            echo ' <td class="ng" >Waiting<img src="'.$pathx.'" width="30pix"; > </td> ';
                            }
                            elseif ($row["status"] == "3" && $row['due_date']>=$datenow) { 
                                $pathx = "assets/images/loading.png";
                            echo ' <td>Waiting<img src="'.$pathx.'" width="30pix"; > </td> ';
                            }
                            if  ($row["status"]== "4" &&  $row['due_date']<$datenow ) {
                                $pathx = "assets/images/greencheck.png";
                            echo ' <td class="ng" >Completed<img src="'.$pathx.'" width="30pix"; > </td> ';
                            }
                            elseif ($row["status"] == "4" && $row['due_date']>=$datenow) {
                            $pathx = "assets/images/greencheck.png";
                            echo '<td>Completed<img src="' . $pathx . '" width="20pix"; > </td> ';
                            }
                            if  ($row["status"]== "5" &&  $row['due_date']<$datenow ) {
                                $pathx = "assets/images/notification-bell.png";
                            echo ' <td class="ng" >Completed<img src="'.$pathx.'" width="30pix"; > </td> ';
                            }
                            elseif ($row["status"] == "5"  && $row['due_date']>=$datenow) {
                            $pathx = "assets/images/notification-bell.png";
                            echo '<td>Feedback<img src="' . $pathx . '" width="20pix"; > </td> ';
                            }
                ?> 
                        <?php } ?>
                        </tr>
    </div>
</div>
</form>

<tr>
   
    <td>
    </td>
    <td>
    </td>
    <td>
  
        <?php
        $sql2 = "SELECT * FROM task INNER JOIN 
        approved on task.task_id = approved.task_id  where approved.member_id = '" .$us. "' ";
        $query2 = mysqli_query($conn, $sql2);
        $total_record = mysqli_num_rows($query2);
        $total_page = ceil($total_record / $perpage);
        if ($total_record > 10) {
        ?>
            <nav aria-label="Page navigation exaple mt-5">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="followup.php?page=1"> Previous</a>
                    </li>
                    <?php for ($i = 1; $i <= $total_page; $i++) { ?>
                        <li class="page-item <?php if ($page == $i) {
                                                    echo 'active';
                                                } ?>">
                            <a class="page-link" href="followup.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li>
                    <?php } ?>
                    <li class="page-item">
                        <a class="page-link" href="followup.php?page=<?php echo $total_page; ?>">
                            Next
                        </a>
                    </li>
                </ul>
            </nav>
        <?php  }   ?>
    </td>
    <td>  
    </td>
</tr>
<script>
    
    $(document).on('click', '#noti', function() {
        var uid = $(this).attr("data-id");

            $.ajax({
                url: "add_noti.php",
                method: "post",
                data: {
                    id: uid
                },
                success: function(data) {
                    location.reload();
                }
            })
    });

    $(document).on('click', '#notiapp', function() {
        var uid = $(this).attr("data-id");
        
        $.ajax({
            url: "add_noti2.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                location.reload();
            }
        })
    });

</script>
</body>
</html>