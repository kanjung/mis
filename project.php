<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Topbar Start -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="chosen/chosen.css">
    <link href="css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <title>Add Project</title>
<!-- end Topbar -->
</head>
<body>
<?php include('template/leftbar.php'); ?>
<?php include('template/topbar.php'); ?>
<div class="main">
    <form class="form-group" action="add_project.php" method="post" id="frmadd" style="padding-top: 5rem ;">
        <div class="form-row">
            <div class="container text-left">Create Project
                <div class="form-group ">
                    <div class="col-lg-7">
                        <h6>Customer :</h6>
                        <input type="text" class="form-control" id="customer" name="customer" placeholder="Customer">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-lg-7">
                        <h6>Project name :</h6>
                        <input type="text" class="form-control" id="project_name" name="project_name" placeholder="Project name">
                        <samp id="show_error" style="color:red;"></samp>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-lg-7">
                        <h6>Tentative Completion Date:</h6>
                        <input placeholder="Tentative Completion" type="text" id="tentative_completion1" name="tentative_completion1" class="form-control" autocomplete="off" disabled>
                        <input type="hidden" id="tentative_completion" name="tentative_completion">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-lg-7">
                        <h6>Project Manager :</h6>
                        <select class="form-control" id="project_manager" name="project_manager" placeholder="project_manager">
                            <option value="">Project Manager</option>
                            <?php
                            $sql1 = "SELECT email_address FROM member ";
                            $query1 = mysqli_query($conn,$sql1);
                            while ($row1 = mysqli_fetch_array($query1, MYSQLI_ASSOC)) {
                            ?>
                                <option value="<?php echo $row1["email_address"]; ?>"><?php echo $row1["email_address"]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-lg-7">
                        <h6>Assigned :</h6>
                        <select data-placeholder="Assigned" class="chosen-select form-control" multiple tabindex="6" id="assigned" name="assigned[]">
                            <option value=""></option>
                            <?php
                            $sql2 = "SELECT * FROM member ";
                            $query2 = mysqli_query($conn, $sql2);
                            while ($row2 = mysqli_fetch_array($query2, MYSQLI_ASSOC)) { ?>
                                <option value="<?php echo $row2["email_address"]; ?>"><?php echo $row2["email_address"]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group text-right">
                    <div class="col-lg-7">
                        <input type="hidden" class="form-control" id="assigned_date" name="assigned_date">
                        <input type="hidden" class="form-control" id="member_id" name="member_id" value="<?php echo $us; ?>">
                        <button type="button" id="addbtn" class="btn btn-dark">Add</button>
                        <a href="view_project.php" type="button" class="btn btn-dark" value="upload">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script> 
<script src="js/gijgo.min.js" type="text/javascript"></script>
<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
<script>

var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#tentative_completion1').datepicker({
        format: 'dd/mm/yyyy',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today
    });

    $(function() {
        $("#customer").autocomplete({
            source: 'employee_show.php',
        });
    });

    $(document).ready(function() {
        $('#addbtn').click(function() {
            
            if ($("#customer").val() == "" && $("#project_name").val() == "" && $("#tentative_completion1").val() == "" && $("#project_manager").val() == "" && $("#assigned").chosen().val() == null) {
                $("#customer").css("border", "1px solid red");
                $("#project_name").css("border", "1px solid red");
                $("#tentative_completion1").css("border", "1px solid red");
                $("#project_manager").css("border", "1px solid red");
                $("#assigned_chosen").css("border", "1px solid red");

                return false;
            }
            if ($("#customer").val() == "") {
                $("#customer").css("border", "1px solid red");
                return false;
            }
            if ($("#project_name").val() == "") {
                $("#project_name").css("border", "1px solid red");
                return false;
            }
            if ($("#tentative_completion1").val() == "") {

                $("#tentative_completion1").css("border", "1px solid red");
                return false;
            }
            if($("#tentative_completion1").val() != "") 
            {
                var te = $("#tentative_completion1").val();
                $("#tentative_completion").val(te);
              //  return false;
            }
                 if ($("#project_manager").val() == "") {

                $("#project_manager").css("border", "1px solid red");
                return false;
            }
           
            if ($("#assigned").chosen().val() == null) {

                $("#assigned_chosen").css("border", "1px solid red");
                return false;
            }

            if ($("#customer").val() != "" && $("#project_name").val() != "") {
                $.ajax({
                    url: "checkproject.php",
                    data: {
                        project_name: $("#project_name").val(),
                        customer: $("#customer").val()
                    },
                    type: "POST",
                    success: function(data) {
                        if (data == 1) {
                            alert(data);
                            $("#show_error").html('มีข้อมูลอยู่แล้ว');
                            $("#project_name").css("border", "1px solid red");
                            return false;
                        }
                        if (data == 0) {

                            $("#frmadd").submit();
                            return false;
                        }
                    }
                });
            }
        });
    });

    $(".chosen-select").chosen()

</script>
</body>
</html>