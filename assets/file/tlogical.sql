-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2020 at 03:05 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tlogical`
--

-- --------------------------------------------------------

--
-- Table structure for table `approved`
--

CREATE TABLE `approved` (
  `app_id` int(20) NOT NULL,
  `approved` varchar(255) NOT NULL,
  `member_id` int(11) NOT NULL,
  `type` int(10) NOT NULL DEFAULT 0,
  `task_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `approved`
--

INSERT INTO `approved` (`app_id`, `approved`, `member_id`, `type`, `task_id`) VALUES
(993, 'rojpiti@tlogical.com', 2, 1, 419),
(994, 'nuttvanun@tlogical.com', 3, 1, 419),
(995, 'rojpiti@tlogical.com', 2, 1, 424),
(996, 'chatchai@tlogical.com', 9, 1, 424),
(997, 'rojpiti@tlogical.com', 2, 1, 425),
(998, 'chatchai@tlogical.com', 9, 1, 425),
(1001, 'rojpiti@tlogical.com', 2, 0, 426),
(1002, 'nuttvanun@tlogical.com', 3, 0, 426),
(1005, 'rojpiti@tlogical.com', 2, 1, 428),
(1006, 'nuttvanun@tlogical.com', 3, 1, 427),
(1007, 'napaporn@tlogical.com', 4, 1, 427),
(1008, 'rojpiti@tlogical.com', 2, 1, 429),
(1010, 'rojpiti@tlogical.com', 2, 1, 431),
(1043, 'rojpiti@tlogical.com', 2, 1, 448),
(1044, 'rojpiti@tlogical.com', 2, 1, 449),
(1045, 'chatchai@tlogical.com', 9, 1, 449),
(1048, 'rojpiti@tlogical.com', 2, 1, 451),
(1049, 'nuttvanun@tlogical.com', 3, 1, 451),
(1050, 'rojpiti@tlogical.com', 2, 1, 452),
(1051, 'rojpiti@tlogical.com', 2, 1, 453),
(1052, 'rojpiti@tlogical.com', 2, 1, 450),
(1054, 'rojpiti@tlogical.com', 2, 0, 454),
(1055, 'rojpiti@tlogical.com', 2, 1, 455),
(1056, 'rojpiti@tlogical.com', 2, 1, 456),
(1057, 'nuttvanun@tlogical.com', 3, 0, 456);

-- --------------------------------------------------------

--
-- Table structure for table `assigned`
--

CREATE TABLE `assigned` (
  `id` int(20) NOT NULL,
  `assigned` varchar(255) DEFAULT NULL,
  `member_id` int(20) DEFAULT NULL,
  `project_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assigned`
--

INSERT INTO `assigned` (`id`, `assigned`, `member_id`, `project_id`) VALUES
(1346, 'songsid@tlogical.com', 7, 265),
(1347, 'faris@tlogical.com', 8, 265),
(1348, 'napaporn@tlogical.com', 4, 266),
(1349, 'rattikan@tlogical.com', 6, 266),
(1350, 'jeharnus@tlogical.com', 11, 266),
(1351, 'songsid@tlogical.com', 7, 267),
(1352, 'faris@tlogical.com', 8, 267),
(1353, 'nuttvanun@tlogical.com', 3, 268),
(1354, 'sirawit@tlogical.com', 5, 268),
(1355, 'songsid@tlogical.com', 7, 269),
(1356, 'faris@tlogical.com', 8, 269),
(1357, 'songsid@tlogical.com', 7, 270),
(1358, 'faris@tlogical.com', 8, 270),
(1359, 'songsid@tlogical.com', 7, 271),
(1360, 'faris@tlogical.com', 8, 271),
(1363, 'songsid@tlogical.com', 7, 273),
(1364, 'faris@tlogical.com', 8, 273),
(1365, 'chatchai@tlogical.com', 9, 273),
(1366, 'songsid@tlogical.com', 7, 274),
(1367, 'faris@tlogical.com', 8, 274),
(1368, 'chatchai@tlogical.com', 9, 274),
(1369, 'taninchote@tlogical.com', 10, 274),
(1370, 'jeharnus@tlogical.com', 11, 274),
(1373, 'napaporn@tlogical.com', 4, 264),
(1374, 'sirawit@tlogical.com', 5, 264),
(1375, 'rattikan@tlogical.com', 6, 264),
(1376, 'jeharnus@tlogical.com', 11, 264),
(1377, 'songsid@tlogical.com', 7, 276),
(1378, 'faris@tlogical.com', 8, 276),
(1379, 'nuttvanun@tlogical.com', 3, 277),
(1380, 'napaporn@tlogical.com', 4, 277),
(1381, 'sirawit@tlogical.com', 5, 277),
(1382, 'taninchote@tlogical.com', 10, 277),
(1385, 'songsid@tlogical.com', 7, 272),
(1386, 'faris@tlogical.com', 8, 272),
(1392, 'chatchai@tlogical.com', 9, 281),
(1393, 'taninchote@tlogical.com', 10, 281),
(1394, 'jeharnus@tlogical.com', 11, 281),
(1401, 'faris@tlogical.com', 8, 284),
(1402, 'chatchai@tlogical.com', 9, 284),
(1405, 'chatchai@tlogical.com', 9, 283),
(1406, 'taninchote@tlogical.com', 10, 283),
(1407, 'jeharnus@tlogical.com', 11, 283),
(1408, 'chatchai@tlogical.com', 9, 279),
(1409, 'taninchote@tlogical.com', 10, 279),
(1410, 'jeharnus@tlogical.com', 11, 279),
(1411, 'chatchai@tlogical.com', 9, 280),
(1412, 'taninchote@tlogical.com', 10, 280),
(1413, 'faris@tlogical.com', 8, 285),
(1414, 'chatchai@tlogical.com', 9, 285),
(1415, 'taninchote@tlogical.com', 10, 285),
(1416, 'songsid@tlogical.com', 7, 285),
(1424, 'chatchai@tlogical.com', 9, 287),
(1425, 'taninchote@tlogical.com', 10, 287),
(1426, 'rattikan@tlogical.com', 6, 287),
(1429, 'chatchai@tlogical.com', 9, 289),
(1430, 'taninchote@tlogical.com', 10, 289),
(1431, 'rojpiti@tlogical.com', 2, 290);

-- --------------------------------------------------------

--
-- Table structure for table `assigned_task`
--

CREATE TABLE `assigned_task` (
  `ass_id` int(20) NOT NULL,
  `assigned` varchar(255) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assigned_task`
--

INSERT INTO `assigned_task` (`ass_id`, `assigned`, `member_id`, `task_id`, `project_id`) VALUES
(327, 'songsid@tlogical.com', 7, 419, 274),
(328, 'faris@tlogical.com', 8, 419, 274),
(329, 'chatchai@tlogical.com', 9, 419, 274),
(330, 'taninchote@tlogical.com', 10, 419, 274),
(331, 'songsid@tlogical.com', 7, 424, 274),
(332, 'faris@tlogical.com', 8, 424, 274),
(333, 'chatchai@tlogical.com', 9, 424, 274),
(334, 'taninchote@tlogical.com', 10, 424, 274),
(335, 'jeharnus@tlogical.com', 11, 424, 274),
(336, 'songsid@tlogical.com', 7, 425, 274),
(337, 'faris@tlogical.com', 8, 425, 274),
(338, 'chatchai@tlogical.com', 9, 425, 274),
(339, 'taninchote@tlogical.com', 10, 425, 274),
(340, 'jeharnus@tlogical.com', 11, 425, 274),
(344, 'faris@tlogical.com', 8, 426, 285),
(345, 'chatchai@tlogical.com', 9, 426, 285),
(346, 'taninchote@tlogical.com', 10, 426, 285),
(350, 'faris@tlogical.com', 8, 428, 285),
(351, 'chatchai@tlogical.com', 9, 428, 285),
(352, 'taninchote@tlogical.com', 10, 428, 285),
(353, 'faris@tlogical.com', 8, 427, 285),
(354, 'chatchai@tlogical.com', 9, 427, 285),
(355, 'taninchote@tlogical.com', 10, 427, 285),
(356, 'songsid@tlogical.com', 7, 429, 274),
(357, 'faris@tlogical.com', 8, 429, 274),
(358, 'chatchai@tlogical.com', 9, 429, 274),
(359, 'taninchote@tlogical.com', 10, 429, 274),
(360, 'jeharnus@tlogical.com', 11, 429, 274),
(364, 'faris@tlogical.com', 8, 431, 285),
(365, 'chatchai@tlogical.com', 9, 431, 285),
(366, 'taninchote@tlogical.com', 10, 431, 285),
(423, 'chatchai@tlogical.com', 9, 448, 287),
(424, 'taninchote@tlogical.com', 10, 448, 287),
(425, 'rattikan@tlogical.com', 6, 448, 287),
(426, 'songsid@tlogical.com', 7, 449, 274),
(427, 'faris@tlogical.com', 8, 449, 274),
(428, 'chatchai@tlogical.com', 9, 449, 274),
(429, 'taninchote@tlogical.com', 10, 449, 274),
(430, 'jeharnus@tlogical.com', 11, 449, 274),
(435, 'chatchai@tlogical.com', 9, 451, 287),
(436, 'taninchote@tlogical.com', 10, 451, 287),
(437, 'rattikan@tlogical.com', 6, 451, 287),
(438, 'chatchai@tlogical.com', 9, 452, 287),
(439, 'taninchote@tlogical.com', 10, 452, 287),
(440, 'rattikan@tlogical.com', 6, 452, 287),
(441, 'chatchai@tlogical.com', 9, 453, 287),
(442, 'taninchote@tlogical.com', 10, 453, 287),
(443, 'rattikan@tlogical.com', 6, 453, 287),
(444, 'faris@tlogical.com', 8, 450, 285),
(445, 'chatchai@tlogical.com', 9, 450, 285),
(446, 'taninchote@tlogical.com', 10, 450, 285),
(450, 'faris@tlogical.com', 8, 454, 287),
(451, 'chatchai@tlogical.com', 9, 454, 287),
(452, 'taninchote@tlogical.com', 10, 454, 287),
(453, 'chatchai@tlogical.com', 9, 455, 287),
(454, 'taninchote@tlogical.com', 10, 455, 287),
(455, 'rattikan@tlogical.com', 6, 455, 287),
(456, 'chatchai@tlogical.com', 9, 456, 289),
(457, 'taninchote@tlogical.com', 10, 456, 289);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cus_id` int(20) NOT NULL,
  `customer_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cus_id`, `customer_name`) VALUES
(93, 'FINNOMENA COMPANY LIMITED'),
(100, 'ลองก๊อง'),
(101, 'Papaya \"co\".,Ltd,'),
(102, 'marongkongkak,\'co;.th'),
(103, 'test'),
(104, 'hgjghj');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(20) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `feed_date` date DEFAULT NULL,
  `upload_file` varchar(255) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `member_id` int(20) DEFAULT NULL,
  `task_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`feedback_id`, `detail`, `feed_date`, `upload_file`, `status`, `member_id`, `task_id`) VALUES
(77, 'ดำเนินการเรียบร้อยแล้ว', '2020-11-10', 'TTN-Maintenance รายการอัพโหลดไฟล์ที่ Error 22.10.20.pdf', 3, 5, 416),
(78, 'dfhdg', '2020-11-10', NULL, 3, 9, 419),
(79, 'sdfdsfsdfsd', '2020-11-10', NULL, 3, 9, 420),
(80, 'sdfgfdgdfg', '2020-11-10', NULL, 3, 9, 421),
(81, '123456', '2020-11-10', NULL, 3, 9, 421),
(82, 'dfghghgh', '2020-11-10', 'insert_plc.php', 3, 9, 0),
(83, 'dsfdsfdsf', '2020-11-10', 'create_plc.php', 3, 9, 0),
(84, 'dsfdsfasdfdsf', '2020-11-10', 'envelope-regular.svg', 3, 9, 0),
(85, 'sdfdsfsds', '2020-11-10', 'dashboard.php', 3, 9, 0),
(86, 'sdfdsfsds', '2020-11-10', 'dashboard.php', 3, 9, 0),
(87, 'sdfdsfsdssdfdsfd', '2020-11-10', 'dashboard.php', 3, 9, 0),
(88, 'sdfdsfsdssdfdsfd', '2020-11-10', 'dashboard.php', 3, 9, 425),
(89, 'sdfdsfdsfsads', '2020-11-10', NULL, 3, 9, 425),
(90, 'sdfdsfdsfsadssadsad', '2020-11-10', 'envelope-regular.svg', 3, 9, 425),
(91, 'csas', '2020-11-10', NULL, 3, 9, 425),
(92, 'sadsadsadsad', '2020-11-10', NULL, 3, 9, 424),
(93, 'ปลอกเปลือก 2 กองกองละ 10 ลูก ลูกละ ยี่สิบ', '2020-11-11', NULL, 3, 9, 428),
(94, 'ปลอกเปลือก ไม่ใช่ ปลอกเปียก ปลอกเปียกมันจะไม่แห้ง\r\nเพราะ ปลอกมันเปียกเลยไม่ได้ปลอกเปลือก', '2020-11-11', NULL, 3, 9, 427),
(95, 'dhdfhggj', '2020-11-11', NULL, 3, 9, 427),
(96, 'จะเห็นว่า เนื่องจาก province_id ของเราเป็น primary key ในแต่ละ geo_id จะมีจังหวัดที่มี\r\nค่า primary key ที่น้อยสุดแสดงเสมอ อย่างเช่น กรุงแทพ ที่มีค่า province_id เท่ากับ 1\r\nหรือก็คือจังหวัดที่เป็นลำดับแรกๆ ของแต่ละภูมิภาค ถูกนำมาแสดง เมื่อมีการใช้งาน GROU', '2020-11-11', NULL, 3, 9, 429),
(97, 'ส่งงานครับ', '2020-11-11', 'envelope-regular.svg', 3, 9, 427),
(98, 'ghgfj', '2020-11-11', NULL, 3, 9, 430),
(99, 'dghtest', '2020-11-11', NULL, 3, 9, 427),
(100, 'sdfdsfdsfdsf', '2020-11-11', NULL, 3, 9, 428),
(101, 'sadfdsfadsf', '2020-11-12', NULL, 3, 9, 428),
(102, 'dsfdsfsdaf', '2020-11-13', 'Hydrangeas.jpg', 3, 9, 427),
(103, '456123', '2020-11-13', 'Tulips.jpg', 3, 9, 427),
(104, 'dsfsfdsfdsf', '2020-11-15', NULL, 3, 9, 448),
(105, 'fgdhdhfdgh', '2020-11-16', 'Desert.jpg', 3, 9, 431),
(106, 'sdafsdafsdafdsaf', '2020-11-16', NULL, 3, 9, 450),
(107, 'dfhddhf', '2020-11-16', NULL, 3, 9, 450),
(108, '1231232131233456', '2020-11-18', 'Hydrangeas.jpg', 3, 9, 431),
(109, '123546', '2020-11-18', 'join.jpeg', 3, 9, 448),
(110, 'Combining AND, OR and NOT\r\nYou can also combine the AND, OR and NOT operators.\r\n\r\nThe following SQL statement selects all fields from \\\"Customers\\\" where country is \\\"Germany\\\" AND city must be \\\"Berlin\\\" OR \\\"München\\\" (use parenthesis to form complex ex', '2020-11-18', '20200729090932675.pdf-2020-08-03-13.pdf', 3, 9, 451),
(111, '123235werewrew', '2020-11-18', '1592377581021.mp4', 3, 9, 453),
(112, 'tatqeqewdcxvfdgerqwq', '2020-11-18', 'Queue management system diagram.pdf', 3, 9, 452),
(113, 'gkhjkrty', '2020-11-18', 'git.txt', 3, 9, 449),
(114, 'dfhfhfdhffd', '2020-11-18', NULL, 3, 9, 449),
(115, 'ghkhjkhjkgh', '2020-11-18', NULL, 3, 9, 449),
(116, 'fdhfhfhfh', '2020-11-18', 'object_detection_webcam.py', 3, 9, 449),
(117, 'hgjghf', '2020-11-18', NULL, 3, 9, 449),
(118, 'hjkhgkh', '2020-11-18', NULL, 3, 9, 449),
(119, 'fgjghjghj', '2020-11-18', NULL, 3, 9, 449),
(120, 'fgdhfh', '2020-11-18', NULL, 3, 9, 449),
(121, 'ghfjgfgj', '2020-11-18', NULL, 3, 9, 449),
(122, 'dsfasdfsadf', '2020-11-18', NULL, 3, 9, 449),
(123, 'sdafasdfsadfsdafsdaf', '2020-11-19', NULL, 3, 9, 449),
(124, 'dfgkjlhjkhk', '2020-11-19', 'object_detection_webcam.py', 3, 9, 449),
(125, 'dfhh', '2020-11-19', NULL, 3, 9, 449),
(126, 'dfhh', '2020-11-19', NULL, 3, 9, 449),
(127, 'dfhh', '2020-11-19', NULL, 3, 9, 449),
(128, 'dsfgdfgdfgdfgd', '2020-11-19', '20200729090932675.pdf-2020-08-03-13.pdf', 3, 9, 449),
(129, 'fghfdhdfhfh', '2020-11-19', NULL, 3, 9, 449),
(130, 'fgjhfgjfgj', '2020-11-19', NULL, 3, 9, 449),
(131, 'dfghdfh', '2020-11-19', NULL, 3, 9, 449),
(132, 'ghkghkgkjklh', '2020-11-19', NULL, 3, 9, 449),
(133, 'hgjfjf', '2020-11-19', NULL, 3, 9, 456),
(134, 'axasxasx', '2020-11-19', NULL, 3, 9, 456);

-- --------------------------------------------------------

--
-- Table structure for table `feedmes`
--

CREATE TABLE `feedmes` (
  `feed_id` int(20) NOT NULL,
  `messager` text DEFAULT NULL,
  `email_address` varchar(250) DEFAULT NULL,
  `upload_file` varchar(255) DEFAULT NULL,
  `member_id` int(20) DEFAULT NULL,
  `task_id` int(20) DEFAULT NULL,
  `feeddate` datetime DEFAULT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedmes`
--

INSERT INTO `feedmes` (`feed_id`, `messager`, `email_address`, `upload_file`, `member_id`, `task_id`, `feeddate`, `project_id`) VALUES
(382, 'dfsg', 'chatchai@tlogical.com', NULL, 9, 421, '2020-11-10 17:13:58', 0),
(383, 'dsfg', 'chatchai@tlogical.com', NULL, 9, 421, '2020-11-10 17:14:00', 0),
(384, 'sd', 'chatchai@tlogical.com', '', 9, 424, '2020-11-10 22:46:13', 0),
(385, 'test', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-11 10:54:44', 0),
(386, 'test', 'chatchai@tlogical.com', '', 9, 427, '2020-11-11 13:48:55', 0),
(387, 'dsfg', 'chatchai@tlogical.com', '', 9, 427, '2020-11-11 13:49:21', 0),
(388, 'fdg', 'chatchai@tlogical.com', '', 9, 427, '2020-11-11 13:51:05', 0),
(389, 'test', 'chatchai@tlogical.com', '', 9, 427, '2020-11-11 13:51:15', 0),
(390, 'er', 'chatchai@tlogical.com', '', 9, 427, '2020-11-11 13:51:21', 0),
(391, 'sdafsdf', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:52:23', 0),
(392, 'ss', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:52:31', 0),
(393, 'qwe', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:52:38', 0),
(394, 'dfg', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:54:50', 0),
(395, 'sdf', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:55:34', 0),
(396, 'fgh', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:56:29', 285),
(397, 'sdf', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 13:57:22', 285),
(398, 'asd', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 14:00:45', 285),
(399, 'asdf', 'chatchai@tlogical.com', '', 9, 428, '2020-11-11 14:01:48', 285),
(400, 'dfg', 'chatchai@tlogical.com', 'create_plc.php', 9, 428, '2020-11-11 14:24:10', 285),
(401, 'fjheee', 'chatchai@tlogical.com', 'mis.sql', 9, 428, '2020-11-11 14:24:53', 285),
(402, 'sdfgf', 'chatchai@tlogical.com', 'mis.sql', 9, 428, '2020-11-11 14:48:26', 285),
(403, 'dhgfhsadf', 'chatchai@tlogical.com', 'envelope-regular.svg', 9, 428, '2020-11-11 14:51:08', 285),
(404, 'dfg', 'chatchai@tlogical.com', 'insert_plc.php', 9, 427, '2020-11-11 14:52:26', 285),
(405, 'dfgdfg', 'chatchai@tlogical.com', 'envelope-regular.svg', 9, 428, '2020-11-11 14:53:07', 285),
(406, 'sdfg', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:54:07', 285),
(407, 'sdfg', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:54:07', 285),
(408, 'sfgsdfg', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:54:15', 285),
(409, 'sfgsdfg', 'chatchai@tlogical.com', 'mis.sql', 9, 428, '2020-11-11 14:54:15', 285),
(410, 'sdfg', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:54:51', 285),
(411, 'TYUIOPdsf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:55:02', 285),
(412, 'sdfdsfds', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:55:06', 285),
(413, 'qq', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:55:12', 285),
(414, 'sdf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:55:28', 285),
(415, 'asdf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 14:55:35', 285),
(416, 'dfhgfh', 'rojpiti@tlogical.com', NULL, 2, 429, '2020-11-11 14:57:43', 0),
(417, '123', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-11 14:58:58', 0),
(418, 'dfhgdhg', 'nuttvanun@tlogical.com', NULL, 3, 427, '2020-11-11 15:47:59', 285),
(419, 'sdfdsf', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-11 15:48:57', 285),
(420, '', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-11 15:48:58', 285),
(421, 'gfh', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-11 15:51:17', 285),
(422, 'fgdfgfg', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 16:09:51', 285),
(423, 'slow', 'chatchai@tlogical.com', NULL, 9, 430, '2020-11-11 16:38:01', 285),
(424, '12', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-11 17:07:27', 0),
(425, '120', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-11 17:07:43', 0),
(426, 'dsfg', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-11 17:08:21', 285),
(427, 'ghk', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-11 17:14:44', 285),
(428, 'หฟก', 'rojpiti@tlogical.com', NULL, 2, 428, '2020-11-11 20:26:50', 285),
(429, 'หฟกหฟก', 'rojpiti@tlogical.com', NULL, 2, 428, '2020-11-11 20:26:57', 285),
(430, 'aga', 'rojpiti@tlogical.com', NULL, 2, 428, '2020-11-11 20:44:30', 0),
(431, 'sdf', 'rojpiti@tlogical.com', NULL, 2, 428, '2020-11-11 20:45:11', 0),
(432, 'dfgdfgf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 21:00:44', 285),
(433, '', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 21:00:45', 285),
(434, 'dfg', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 21:00:48', 285),
(435, 'dsfdsf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 21:03:16', 285),
(436, 'dsfsdfsd', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-11 21:03:18', 285),
(437, 'ghkjhk', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:11:09', 285),
(438, 'ghkjhk', 'chatchai@tlogical.com', 'create_plc.php', 9, 428, '2020-11-12 09:11:09', 285),
(439, 'gml', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:18:51', 285),
(440, 'gml', 'chatchai@tlogical.com', 'josh-spires-dronenr-Xrl6D5UqiNA-unsplash.jpg', 9, 428, '2020-11-12 09:18:52', 285),
(441, 'ghtiktdyh', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:45:07', 285),
(442, 'ghtiktdyh', 'chatchai@tlogical.com', 'yuriy-mlcn-OCNAFoV2QzQ-unsplash.jpg', 9, 428, '2020-11-12 09:45:08', 285),
(443, 'เทส', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:51:40', 285),
(444, '่้า่้า', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:52:34', 285),
(445, '่้า่้า', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:52:36', 285),
(446, '้ห้ห้ะ่ะก', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:52:46', 285),
(447, 'ีีรีรร้่ร', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 09:54:11', 285),
(448, 'tuirtuyrwttu', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 10:19:35', 285),
(449, 'kghl,fgdghjfh', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 10:43:23', 285),
(450, '52', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-12 11:46:47', 285),
(451, '5252', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-12 11:47:03', 285),
(452, '5252', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 11:49:35', 285),
(453, '546546546', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 11:50:16', 285),
(454, '456', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 11:50:20', 285),
(455, '', '', NULL, 0, 0, '2020-11-12 13:20:51', 0),
(456, '', '', NULL, 0, 0, '2020-11-12 13:20:54', 0),
(457, '', '', NULL, 0, 0, '2020-11-12 13:20:55', 0),
(458, '', '', NULL, 0, 0, '2020-11-12 13:20:56', 0),
(459, '', '', NULL, 0, 0, '2020-11-12 13:20:56', 0),
(460, '', '', NULL, 0, 0, '2020-11-12 13:20:56', 0),
(461, '', '', NULL, 0, 0, '2020-11-12 13:20:57', 0),
(462, '', '', NULL, 0, 0, '2020-11-12 13:20:57', 0),
(463, '', '', NULL, 0, 0, '2020-11-12 13:20:57', 0),
(464, '', '', NULL, 0, 0, '2020-11-12 13:28:01', 0),
(465, '', '', NULL, 0, 0, '2020-11-12 13:31:40', 0),
(466, '', '', NULL, 0, 0, '2020-11-12 13:32:42', 0),
(467, '', '', NULL, 0, 0, '2020-11-12 13:34:24', 0),
(468, '', '', NULL, 0, 0, '2020-11-12 13:34:50', 0),
(469, '', '', NULL, 0, 0, '2020-11-12 13:35:23', 0),
(470, '', '', NULL, 0, 0, '2020-11-12 13:35:37', 0),
(471, '', '', NULL, 0, 0, '2020-11-12 13:37:35', 0),
(472, '', '', NULL, 0, 0, '2020-11-12 13:37:49', 0),
(473, '', '', NULL, 0, 0, '2020-11-12 13:39:27', 0),
(474, '', '', NULL, 0, 0, '2020-11-12 13:40:07', 0),
(475, '', '', NULL, 0, 0, '2020-11-12 13:40:10', 0),
(476, '', '', NULL, 0, 0, '2020-11-12 13:41:28', 0),
(477, '', '', NULL, 0, 0, '2020-11-12 13:47:11', 0),
(478, '', '', NULL, 0, 0, '2020-11-12 13:47:33', 0),
(479, '', '', NULL, 0, 0, '2020-11-12 13:47:33', 0),
(480, '', '', NULL, 0, 0, '2020-11-12 13:47:34', 0),
(481, '', '', NULL, 0, 0, '2020-11-12 13:47:34', 0),
(482, '', '', NULL, 0, 0, '2020-11-12 13:47:34', 0),
(483, '', '', NULL, 0, 0, '2020-11-12 13:48:06', 0),
(484, '', '', NULL, 0, 0, '2020-11-12 13:49:34', 0),
(485, '', '', NULL, 0, 0, '2020-11-12 13:50:32', 0),
(486, '', '', NULL, 0, 0, '2020-11-12 13:51:57', 0),
(487, '', '', NULL, 0, 0, '2020-11-12 13:52:40', 0),
(488, '', '', NULL, 0, 0, '2020-11-12 13:52:44', 0),
(489, '', '', NULL, 0, 0, '2020-11-12 13:56:15', 0),
(490, '', '', NULL, 0, 0, '2020-11-12 13:59:12', 0),
(491, '', '', NULL, 0, 0, '2020-11-12 13:59:15', 0),
(492, '', '', NULL, 0, 0, '2020-11-12 14:01:52', 0),
(493, '', '', NULL, 0, 0, '2020-11-12 14:02:28', 0),
(494, '', '', NULL, 0, 0, '2020-11-12 14:04:21', 0),
(495, '', '', NULL, 0, 0, '2020-11-12 14:04:49', 0),
(496, '', '', 'samuel-scrimshaw-fkS-me35j7I-unsplash.jpg', 0, 0, '2020-11-12 14:06:54', 0),
(497, '', '', NULL, 0, 0, '2020-11-12 14:07:35', 0),
(498, '', '', NULL, 0, 0, '2020-11-12 14:11:55', 0),
(499, '', '', NULL, 0, 0, '2020-11-12 14:14:59', 0),
(500, '', '', NULL, 0, 0, '2020-11-12 14:17:02', 0),
(501, '', '', NULL, 0, 0, '2020-11-12 14:17:56', 0),
(502, '', '', NULL, 0, 0, '2020-11-12 14:31:13', 0),
(503, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:39:37', 0),
(504, '', '', 'yuriy-mlcn-OCNAFoV2QzQ-unsplash.jpg', 0, 0, '2020-11-12 14:40:12', 0),
(505, '', '', NULL, 0, 0, '2020-11-12 14:40:21', 0),
(506, '', '', 'samuel-scrimshaw-fkS-me35j7I-unsplash.jpg', 0, 0, '2020-11-12 14:40:44', 0),
(507, '', '', 'reuben-teo-8JzoJyt3hyM-unsplash.jpg', 0, 0, '2020-11-12 14:42:36', 0),
(508, '', '', NULL, 0, 0, '2020-11-12 14:44:01', 0),
(509, '', '', NULL, 0, 0, '2020-11-12 14:44:45', 0),
(510, '', '', 'roberto-nickson-Jat5D3lH_FA-unsplash.jpg', 0, 0, '2020-11-12 14:45:10', 0),
(511, '', '', 'yuriy-mlcn-OCNAFoV2QzQ-unsplash.jpg', 0, 0, '2020-11-12 14:45:41', 0),
(512, '', '', 'yuriy-mlcn-OCNAFoV2QzQ-unsplash.jpg', 0, 0, '2020-11-12 14:46:16', 0),
(513, '', '', NULL, 0, 0, '2020-11-12 14:46:28', 0),
(514, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:48', 0),
(515, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:49', 0),
(516, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:50', 0),
(517, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:50', 0),
(518, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:50', 0),
(519, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:50', 0),
(520, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:51', 0),
(521, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:51', 0),
(522, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:51', 0),
(523, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:51', 0),
(524, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:46:52', 0),
(525, '', '', 'reuben-teo-8JzoJyt3hyM-unsplash.jpg', 0, 0, '2020-11-12 14:47:16', 0),
(526, '', '', NULL, 0, 0, '2020-11-12 14:54:24', 0),
(527, '', '', 'tlogical_main.sql', 0, 0, '2020-11-12 14:54:32', 0),
(528, '', '', NULL, 0, 0, '2020-11-12 14:55:44', 0),
(529, '', '', NULL, 0, 0, '2020-11-12 14:55:54', 0),
(530, '', '', NULL, 0, 0, '2020-11-12 15:05:18', 0),
(531, '', '', NULL, 0, 0, '2020-11-12 15:05:22', 0),
(532, '', '', NULL, 0, 0, '2020-11-12 15:05:22', 0),
(533, '', '', NULL, 0, 0, '2020-11-12 15:05:22', 0),
(534, '', '', NULL, 0, 0, '2020-11-12 15:05:22', 0),
(535, '', '', NULL, 0, 0, '2020-11-12 15:05:23', 0),
(536, '', '', NULL, 0, 0, '2020-11-12 15:05:23', 0),
(537, '', '', NULL, 0, 0, '2020-11-12 15:05:23', 0),
(538, '', '', NULL, 0, 0, '2020-11-12 15:05:24', 0),
(539, '', '', NULL, 0, 0, '2020-11-12 15:05:24', 0),
(540, '', '', NULL, 0, 0, '2020-11-12 15:05:24', 0),
(541, '', '', NULL, 0, 0, '2020-11-12 15:05:24', 0),
(542, '', '', NULL, 0, 0, '2020-11-12 15:05:24', 0),
(543, '', '', NULL, 0, 0, '2020-11-12 15:05:24', 0),
(544, '', '', NULL, 0, 0, '2020-11-12 15:05:42', 0),
(545, '', '', NULL, 0, 0, '2020-11-12 15:05:49', 0),
(546, 'dfssfsfsff', 'chatchai@tlogical.com', 'tlogical_main.sql', 9, 0, '2020-11-12 15:20:10', 285),
(547, 'dfssffsfs', 'chatchai@tlogical.com', 'TLO-Thecampus 200924.xlsx', 9, 0, '2020-11-12 15:22:46', 285),
(548, 'sffsffssf', 'chatchai@tlogical.com', 'TTE-TechBook-200915.xd', 9, 0, '2020-11-12 15:24:10', 285),
(549, 'sfsffssf', 'chatchai@tlogical.com', 'TLO-TheCampus-UXUI-EN(Student)-201007.xd', 9, 428, '2020-11-12 15:25:39', 285),
(550, 'r5sdgdafg', 'chatchai@tlogical.com', 'yuriy-mlcn-OCNAFoV2QzQ-unsplash.jpg', 9, 428, '2020-11-12 15:29:12', 285),
(551, 'สวัสดีปีใหม่', 'chatchai@tlogical.com', '1.jpg', 9, 428, '2020-11-12 15:29:39', 285),
(552, 'sdfdsfewf', 'chatchai@tlogical.com', 'Home-ผสาน.pdf', 9, 428, '2020-11-12 16:54:36', 285),
(553, 'gfhfgh', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-12 16:56:33', 285),
(554, 'gfhfgh', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-12 16:56:50', 285),
(555, 'sdfsdf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 16:58:00', 285),
(556, 'dgh', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 17:00:53', 285),
(557, 'fgh', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 17:00:56', 285),
(558, 'fghfgh', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 17:01:01', 285),
(559, 'sdafadsf', 'chatchai@tlogical.com', NULL, 9, 428, '2020-11-12 19:39:31', 285),
(560, 'sadfdsf', 'chatchai@tlogical.com', 'join.jpeg', 9, 428, '2020-11-12 19:39:49', 285),
(561, 'sadsad', 'rojpiti@tlogical.com', NULL, 2, 428, '2020-11-12 19:42:59', 285),
(562, 'sadsadsa', 'rojpiti@tlogical.com', 'PDPA.txt', 2, 428, '2020-11-12 19:43:27', 285),
(563, 'sadsadas', 'rojpiti@tlogical.com', NULL, 2, 428, '2020-11-12 19:43:39', 285),
(564, 'msa', 'rojpiti@tlogical.com', 'Desert.jpg', 2, 428, '2020-11-12 19:44:01', 285),
(565, 'ASD', 'rojpiti@tlogical.com', 'Koala.jpg', 2, 431, '2020-11-12 19:44:38', 285),
(566, 'kaalo koala', 'napaporn@tlogical.com', 'Koala.jpg', 4, 427, '2020-11-13 09:50:45', 285),
(567, 'sdfg', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-13 09:52:35', 285),
(568, 'sdf', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-13 09:52:48', 285),
(569, 'dfgh', 'napaporn@tlogical.com', NULL, 4, 0, '2020-11-13 09:53:03', 285),
(570, 'sdfgfdg', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-13 09:53:46', 285),
(571, 'บ้านๆ', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-13 09:54:08', 285),
(572, 'ดเ่', 'napaporn@tlogical.com', 'Jellyfish.jpg', 4, 427, '2020-11-13 09:55:26', 0),
(573, 'บ้าน', 'napaporn@tlogical.com', 'Lighthouse.jpg', 4, 427, '2020-11-13 09:55:41', 0),
(574, 'fdhfd', 'chatchai@tlogical.com', 'Chrysanthemum.jpg', 9, 427, '2020-11-13 10:46:55', 285),
(575, '456', 'napaporn@tlogical.com', NULL, 4, 427, '2020-11-13 11:22:12', 0),
(576, 'fgj', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-13 16:43:50', 285),
(577, 'dfh', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-13 16:44:04', 285),
(578, '123', 'chatchai@tlogical.com', NULL, 9, 427, '2020-11-13 16:47:58', 285),
(579, 'jhgkhjk', 'chatchai@tlogical.com', NULL, 9, 431, '2020-11-13 16:55:37', 285),
(580, 'gfhtest', 'chatchai@tlogical.com', 'Tulips.jpg', 9, 432, '2020-11-13 16:58:51', 287),
(581, 'gagsgr', 'rojpiti@tlogical.com', 'Hydrangeas.jpg', 2, 448, '2020-11-16 08:38:10', 0),
(582, 'sdfgsdfgf', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-16 11:07:10', 0),
(583, 'fgdhghh', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 09:14:15', 285),
(584, '4568787', 'rojpiti@tlogical.com', NULL, 2, 431, '2020-11-18 09:15:02', 0),
(585, 'testtest', 'rojpiti@tlogical.com', NULL, 2, 450, '2020-11-18 10:26:41', 0),
(586, 'fdhdfh', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 11:28:13', 285),
(587, 'fgjfjj', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 11:29:42', 285),
(588, '123123', 'rojpiti@tlogical.com', NULL, 2, 450, '2020-11-18 13:26:20', 285),
(589, 'fgjhtest', 'rojpiti@tlogical.com', NULL, 2, 450, '2020-11-18 13:29:19', 285),
(590, 'fgjfgjfgje45435', 'chatchai@tlogical.com', 'Jellyfish.jpg', 9, 450, '2020-11-18 13:30:57', 285),
(591, 'fgfkfgkfgk', 'rojpiti@tlogical.com', '(NEIS) ระบบตรวจสอบการทำงานของระบบเครือข่าย และแจ้งเตือนผ่านแอพพลิเคชั่น LINE.pdf', 2, 449, '2020-11-18 17:00:31', 274),
(592, 'ttestese', 'rojpiti@tlogical.com', 'PDPA.txt', 2, 452, '2020-11-18 17:01:58', 0),
(593, 'sadfsadfsda', 'chatchai@tlogical.com', 'date.jpg', 9, 449, '2020-11-18 18:30:38', 0),
(594, 'sdafsdfsdf', 'chatchai@tlogical.com', NULL, 9, 449, '2020-11-18 18:34:12', 0),
(595, 'gogo', 'chatchai@tlogical.com', NULL, 9, 449, '2020-11-18 18:41:31', 0),
(596, 'xcvcxvcx', 'chatchai@tlogical.com', NULL, 9, 449, '2020-11-18 18:46:05', 0),
(597, 'dsfg', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 19:03:40', 0),
(598, 'fdhfh', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 19:04:41', 285),
(599, 'fghgh', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 19:04:53', 285),
(600, 'ghjghjg', 'chatchai@tlogical.com', NULL, 9, 450, '2020-11-18 20:37:39', 285),
(601, 'fghdfhfhf', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:07:38', 274),
(602, 'dfghdfhg', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:11:19', 274),
(603, 'ghjgfjgj', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:11:29', 274),
(604, 'rtertert', 'rojpiti@tlogical.com', 'join.jpeg', 2, 449, '2020-11-19 09:19:54', 274),
(605, 'gfj', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:24:04', 274),
(606, 'dfhdfh', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:26:43', 274),
(607, 'dfhdhd', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:27:14', 274),
(608, 'fgjgj', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:27:49', 274),
(609, 'gfjfgjf', 'rojpiti@tlogical.com', NULL, 2, 449, '2020-11-19 09:28:18', 274),
(610, 'fgjgj', 'rojpiti@tlogical.com', NULL, 2, 456, '2020-11-19 09:38:48', 289),
(611, 'sdfgdfgsdfg', 'chatchai@tlogical.com', NULL, 9, 456, '2020-11-19 10:08:12', 289),
(612, '', 'chatchai@tlogical.com', NULL, 9, 456, '2020-11-19 10:08:14', 289),
(613, '', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-25 15:18:36', 289),
(614, '', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-25 15:18:40', 289),
(615, '', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-25 15:18:44', 289),
(616, '', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-25 15:18:44', 289),
(617, '', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-25 15:18:45', 289),
(618, '', 'chatchai@tlogical.com', NULL, 9, 0, '2020-11-25 15:38:01', 289);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(15) NOT NULL,
  `emp_id` varchar(15) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `position` varchar(255) NOT NULL,
  `inbox` varchar(255) DEFAULT NULL,
  `type` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `emp_id`, `first_name`, `last_name`, `pwd`, `email_address`, `position`, `inbox`, `type`) VALUES
(2, 'T15001', 'นายโรจน์ปิติ\r\n', 'ธรรมชูเวท\r\n', '123456', 'rojpiti@tlogical.com', 'Chief Executive Officer (CEO)\r\n', NULL, 1),
(3, 'T15002', 'น.ส.นัทวนันต์\r\n', 'ปรมะเจริญโรจน์\r\n', '123456', 'nuttvanun@tlogical.com', 'Chief Growth Officer\r\n', NULL, 1),
(4, 'T18001', 'น.ส.นภาพร\r\n', 'จูสุวรรณ์\r\n', '123456', 'napaporn@tlogical.com', 'Project Coordinate\r\n', NULL, NULL),
(5, 'T19004', 'นายสิรวิชญ์ \r\n', 'กรีพร\r\n', '123456', 'sirawit@tlogical.com', 'Strategic Planner\r\n', NULL, NULL),
(6, 'T19006', 'น.ส.รัตติกาล\r\n', 'กันใจมา\r\n', '123456', 'rattikan@tlogical.com', 'Programmer\r\n', NULL, NULL),
(7, 'T19007', 'นายทรงสิทธิ์\r\n', 'กองแก้ว\r\n', '123456', 'songsid@tlogical.com', 'Network & Security Engineer\r\n', NULL, NULL),
(8, 'T19008', 'นายฟารีด\r\n', 'หมานเหล็บ\r\n', '123456', 'faris@tlogical.com', 'Network & Security Engineer\r\n', NULL, NULL),
(9, 'T20002', 'นายฉัตรชัย\r\n', 'บูรณวิเศษกุล\r\n', '123456', 'chatchai@tlogical.com', 'Programmer\r\n', NULL, NULL),
(10, 'T20003', 'นาย ธนินท์โชติ \r\n', 'อนันต์มงคลชัย\r\n', '123456', 'taninchote@tlogical.com', 'Creative motion graphic designer\r\n', NULL, NULL),
(11, 'T20004', 'นายเจ๊ะอานัส\r\n', 'ม่องพร้า\r\n', '123456', 'jeharnus@tlogical.com', 'Programmer\r\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `no_id` int(20) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `to_id` int(20) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `clickdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `task_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`no_id`, `message`, `to_id`, `from_id`, `status`, `clickdate`, `enddate`, `task_id`) VALUES
(2137, 'กรุณาเข้าไปตรวจงานghjgj', 2, 9, 0, '2020-11-18', '2020-11-19', 455),
(2138, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2139, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2140, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2141, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2142, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2143, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2144, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2145, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2146, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2147, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2148, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2149, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2150, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2151, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2152, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2153, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2154, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2155, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2156, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2157, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2158, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2159, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2160, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2161, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2162, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2163, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2164, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2165, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2166, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2167, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2168, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2169, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2170, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2171, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2172, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2173, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2174, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2175, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2176, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2177, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2178, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2179, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2180, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2181, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2182, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2183, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2184, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2185, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2186, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 7, 2, 0, '2020-11-19', '2020-11-20', 449),
(2187, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 8, 2, 0, '2020-11-19', '2020-11-20', 449),
(2188, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 0, '2020-11-19', '2020-11-20', 449),
(2189, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 449),
(2190, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 11, 2, 0, '2020-11-19', '2020-11-20', 449),
(2191, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2192, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2193, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2194, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2195, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2196, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2197, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2198, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449),
(2199, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2200, 'มีงานเข้ามารอการตรวจ', 9, 9, 1, '2020-11-19', '2020-11-20', 449),
(2201, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 2, 1, '2020-11-19', '2020-11-20', 456),
(2202, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 2, 0, '2020-11-19', '2020-11-20', 456),
(2203, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 9, 0, '2020-11-19', '2020-11-20', 456),
(2204, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 9, 0, '2020-11-19', '2020-11-20', 456),
(2205, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 9, 9, 1, '2020-11-19', '2020-11-20', 456),
(2206, 'กรุณาเข้าไปตรวจสอบ Feedback ของคุณ', 10, 9, 0, '2020-11-19', '2020-11-20', 456),
(2207, 'มีงานเข้ามารอการตรวจ', 2, 9, 0, '2020-11-19', '2020-11-20', 449),
(2208, 'มีงานเข้ามารอการตรวจ', 9, 9, 0, '2020-11-19', '2020-11-20', 449);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(15) NOT NULL,
  `assigned_date` date DEFAULT NULL,
  `tentative_completion` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `status` int(15) NOT NULL DEFAULT 0,
  `project_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `customer` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `project_manager` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `assigned_date`, `tentative_completion`, `deadline`, `status`, `project_name`, `type`, `customer`, `project_manager`, `member_id`, `task_id`) VALUES
(274, '2020-11-10', '2020-12-11', NULL, 4, 'FNM | Display System', 2, '93', 'rojpiti@tlogical.com', 4, NULL),
(285, '2020-11-11', '2020-11-21', NULL, 4, 'ลองก๊อง\'.,๔ู฿$%^&*()_=-]\'[\';}:\"}', 0, '100', 'rojpiti@tlogical.com', 9, NULL),
(287, '2020-11-13', '2020-11-21', NULL, 4, 'Mariskiskron!!--++__))((**^&#$%', 2, '102', 'rojpiti@tlogical.com', 9, NULL),
(289, '2020-11-19', '2020-11-21', NULL, 0, 'test', 0, '103', 'rojpiti@tlogical.com', 2, NULL),
(290, '2020-11-19', '2020-11-21', NULL, 0, 'hgjghj', 0, '104', 'rojpiti@tlogical.com', 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(15) NOT NULL,
  `client` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `project` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `detail` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `upload_file` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `customer_request` int(11) DEFAULT 0,
  `submission` date DEFAULT NULL,
  `member_id` int(15) DEFAULT NULL,
  `assigned_date` date DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT 1,
  `project_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `client`, `project`, `detail`, `subject`, `due_date`, `upload_file`, `type`, `customer_request`, `submission`, `member_id`, `assigned_date`, `status`, `project_id`) VALUES
(419, ' 93', 'FNM | Display System', 'dfhdgasdfds', 'hgj', '2020-11-14', NULL, NULL, 0, NULL, 9, '2020-11-10', 4, 274),
(424, '93', 'FNM | Display System', 'sadsadsadsad', 'werewr', '2020-11-14', NULL, NULL, 1, NULL, 9, '2020-11-10', 4, 274),
(425, '93', 'FNM | Display System', 'csas', 'sadfd', '2020-11-14', NULL, NULL, 1, NULL, 9, '2020-11-10', 4, 274),
(427, ' 100', 'ลองก๊อง\'.,๔ู฿$%^&*()_=-]\'[\';}:\"}?><>,.//||', 'แยกกองลองก๊อง', 'ปลอกเปียก', '2020-11-14', 'create_plc.php', NULL, 0, NULL, 9, '2020-11-11', 4, 285),
(428, '100', 'ลองก๊อง\'.,๔ู฿$%^&*()_=-]\'[\';}:\"}?><>,.//||', ' ปลอกเปลือก ไม่ใช่ ปลอกเปียก ลองก๊อง', 'ผลไม้', '2020-11-10', NULL, NULL, 0, NULL, 9, '2020-11-11', 4, 285),
(429, '93', 'FNM | Display System', 'dsafkl&*()_}\"?./][', 'projecadfsdfsdf ', '2020-11-14', NULL, NULL, 1, NULL, 9, '2020-11-11', 4, 274),
(431, '100', 'ลองก๊อง\'.,๔ู฿$%^&*()_=-]\'[\';}:\"}?><>,.//||', 'dsfsdfsd', 'sdfdsf', '2020-11-10', NULL, NULL, 0, NULL, 9, '2020-11-11', 4, 285),
(448, '102', 'Mariskiskron!!--++__))((**^&#$%', 'dsfadsfds', 'sadfsdf', '2020-11-21', NULL, NULL, 0, NULL, 9, '2020-11-15', 4, 287),
(449, '93', 'FNM | Display System', 'ghgdh', 'dfhf', '2020-11-21', NULL, NULL, 1, NULL, 2, '2020-11-16', 4, 274),
(450, ' 100', 'ลองก๊อง\'.,๔ู฿$%^&*()_=-]\'[\';}:\"}', 'dfghdf', 'fgh', '2020-11-21', 'join.jpeg', NULL, 0, NULL, 2, '2020-11-16', 4, 285),
(451, '102', 'Mariskiskron!!--++__))((**^&#$%', 'gdfsgsdfgdfsgdsfgsdfg', 'sgsdfgsdf', '2020-11-21', 'Koala.jpg', NULL, 0, NULL, 9, '2020-11-18', 4, 287),
(452, '102', 'Mariskiskron!!--++__))((**^&#$%', 'ASDASDASDS', 'sadaD', '2020-11-21', 'PDPA.txt', NULL, 0, NULL, 9, '2020-11-18', 4, 287),
(453, '102', 'Mariskiskron!!--++__))((**^&#$%', 'dfgdfg', 'sdfgdfg', '2020-11-28', 'MIS Timeline.xlsx', NULL, 0, NULL, 9, '2020-11-18', 4, 287),
(455, '102', 'Mariskiskron!!--++__))((**^&#$%', 'ghj', 'ghjgj', '2020-11-21', NULL, NULL, 1, NULL, 9, '2020-11-18', 4, 287),
(456, '103', 'test', 'test', 'test', '2020-11-21', 'MIS Timeline.xlsx', NULL, 0, NULL, 2, '2020-11-19', 3, 289);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `approved`
--
ALTER TABLE `approved`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `assigned`
--
ALTER TABLE `assigned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assigned_task`
--
ALTER TABLE `assigned_task`
  ADD PRIMARY KEY (`ass_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `feedmes`
--
ALTER TABLE `feedmes`
  ADD PRIMARY KEY (`feed_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`no_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `approved`
--
ALTER TABLE `approved`
  MODIFY `app_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1058;

--
-- AUTO_INCREMENT for table `assigned`
--
ALTER TABLE `assigned`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1432;

--
-- AUTO_INCREMENT for table `assigned_task`
--
ALTER TABLE `assigned_task`
  MODIFY `ass_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=458;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `cus_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `feedmes`
--
ALTER TABLE `feedmes`
  MODIFY `feed_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=619;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `no_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2209;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=457;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`),
  ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
