-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2020 at 10:30 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ttt`
--

-- --------------------------------------------------------

--
-- Table structure for table `approved`
--

CREATE TABLE `approved` (
  `app_id` int(20) NOT NULL,
  `approved` varchar(255) NOT NULL,
  `member_id` int(11) NOT NULL,
  `type` int(10) NOT NULL DEFAULT 0,
  `task_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `approved`
--

INSERT INTO `approved` (`app_id`, `approved`, `member_id`, `type`, `task_id`) VALUES
(892, 'rojpiti@tlogical.com', 2, 0, 0),
(893, 'nuttvanun@tlogical.com', 3, 0, 0),
(894, 'rojpiti@tlogical.com', 2, 0, 0),
(895, 'nuttvanun@tlogical.com', 3, 0, 0),
(896, 'rojpiti@tlogical.com', 2, 0, 0),
(897, 'nuttvanun@tlogical.com', 3, 0, 0),
(898, 'rojpiti@tlogical.com', 2, 0, 0),
(899, 'nuttvanun@tlogical.com', 3, 0, 0),
(900, 'rojpiti@tlogical.com', 2, 0, 0),
(901, 'napaporn@tlogical.com', 4, 0, 0),
(902, 'rojpiti@tlogical.com', 2, 0, 0),
(903, 'rojpiti@tlogical.com', 2, 0, 0),
(906, 'rojpiti@tlogical.com', 2, 0, 416),
(907, 'nuttvanun@tlogical.com', 3, 0, 416),
(915, 'rojpiti@tlogical.com', 2, 0, 418),
(916, 'nuttvanun@tlogical.com', 3, 0, 418),
(917, 'faris@tlogical.com', 8, 0, 418),
(918, 'rojpiti@tlogical.com', 2, 0, 417);

-- --------------------------------------------------------

--
-- Table structure for table `assigned`
--

CREATE TABLE `assigned` (
  `id` int(20) NOT NULL,
  `assigned` varchar(255) DEFAULT NULL,
  `member_id` int(20) DEFAULT NULL,
  `project_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assigned`
--

INSERT INTO `assigned` (`id`, `assigned`, `member_id`, `project_id`) VALUES
(1346, 'songsid@tlogical.com', 7, 265),
(1347, 'faris@tlogical.com', 8, 265),
(1348, 'napaporn@tlogical.com', 4, 266),
(1349, 'rattikan@tlogical.com', 6, 266),
(1350, 'jeharnus@tlogical.com', 11, 266),
(1351, 'songsid@tlogical.com', 7, 267),
(1352, 'faris@tlogical.com', 8, 267),
(1353, 'nuttvanun@tlogical.com', 3, 268),
(1354, 'sirawit@tlogical.com', 5, 268),
(1355, 'songsid@tlogical.com', 7, 269),
(1356, 'faris@tlogical.com', 8, 269),
(1357, 'songsid@tlogical.com', 7, 270),
(1358, 'faris@tlogical.com', 8, 270),
(1359, 'songsid@tlogical.com', 7, 271),
(1360, 'faris@tlogical.com', 8, 271),
(1363, 'songsid@tlogical.com', 7, 273),
(1364, 'faris@tlogical.com', 8, 273),
(1365, 'chatchai@tlogical.com', 9, 273),
(1366, 'songsid@tlogical.com', 7, 274),
(1367, 'faris@tlogical.com', 8, 274),
(1368, 'chatchai@tlogical.com', 9, 274),
(1369, 'taninchote@tlogical.com', 10, 274),
(1370, 'jeharnus@tlogical.com', 11, 274),
(1373, 'napaporn@tlogical.com', 4, 264),
(1374, 'sirawit@tlogical.com', 5, 264),
(1375, 'rattikan@tlogical.com', 6, 264),
(1376, 'jeharnus@tlogical.com', 11, 264),
(1377, 'songsid@tlogical.com', 7, 276),
(1378, 'faris@tlogical.com', 8, 276),
(1379, 'nuttvanun@tlogical.com', 3, 277),
(1380, 'napaporn@tlogical.com', 4, 277),
(1381, 'sirawit@tlogical.com', 5, 277),
(1382, 'taninchote@tlogical.com', 10, 277),
(1385, 'songsid@tlogical.com', 7, 272),
(1386, 'faris@tlogical.com', 8, 272);

-- --------------------------------------------------------

--
-- Table structure for table `assigned_task`
--

CREATE TABLE `assigned_task` (
  `ass_id` int(20) NOT NULL,
  `assigned` varchar(255) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assigned_task`
--

INSERT INTO `assigned_task` (`ass_id`, `assigned`, `member_id`, `task_id`, `project_id`) VALUES
(272, 'sirawit@tlogical.com', 5, 0, 264),
(273, 'sirawit@tlogical.com', 5, 0, 264),
(274, 'rattikan@tlogical.com', 6, 0, 266),
(275, 'songsid@tlogical.com', 7, 0, 269),
(276, 'nuttvanun@tlogical.com', 3, 0, 277),
(277, 'napaporn@tlogical.com', 4, 0, 277),
(278, 'faris@tlogical.com', 8, 0, 273),
(279, 'faris@tlogical.com', 8, 0, 273),
(281, 'sirawit@tlogical.com', 5, 416, 264),
(289, 'songsid@tlogical.com', 7, 418, 270),
(290, 'rattikan@tlogical.com', 6, 417, 268);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cus_id` int(20) NOT NULL,
  `customer_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cus_id`, `customer_name`) VALUES
(89, 'Thanatham Education Co.,Ltd.'),
(90, 'T.logical Resolution Co.,Ltd.'),
(91, 'Khun.Baz'),
(92, 'บริษัท กสท โทรคมนาคม จำกัด​​ (มหาชน)'),
(93, 'FINNOMENA COMPANY LIMITED'),
(94, 'MEDINT\' \"Co\".,Ltd.'),
(95, 'บริษัท เซ็น แอนด์ สไปซี่ จำกัด');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(20) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `feed_date` date DEFAULT NULL,
  `upload_file` varchar(255) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `member_id` int(20) DEFAULT NULL,
  `task_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`feedback_id`, `detail`, `feed_date`, `upload_file`, `status`, `member_id`, `task_id`) VALUES
(77, 'ดำเนินการเรียบร้อยแล้ว', '2020-11-10', 'TTN-Maintenance รายการอัพโหลดไฟล์ที่ Error 22.10.20.pdf', 3, 5, 416);

-- --------------------------------------------------------

--
-- Table structure for table `feedmes`
--

CREATE TABLE `feedmes` (
  `feed_id` int(20) NOT NULL,
  `messager` text DEFAULT NULL,
  `email_address` varchar(250) DEFAULT NULL,
  `upload_file` varchar(255) DEFAULT NULL,
  `member_id` int(20) DEFAULT NULL,
  `task_id` int(20) DEFAULT NULL,
  `feeddate` datetime DEFAULT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(15) NOT NULL,
  `emp_id` varchar(15) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `position` varchar(255) NOT NULL,
  `inbox` varchar(255) DEFAULT NULL,
  `type` int(12) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `emp_id`, `first_name`, `last_name`, `pwd`, `email_address`, `position`, `inbox`, `type`) VALUES
(2, 'T15001', 'นายโรจน์ปิติ\r\n', 'ธรรมชูเวท\r\n', '123456', 'rojpiti@tlogical.com', 'Chief Executive Officer (CEO)\r\n', NULL, 1),
(3, 'T15002', 'น.ส.นัทวนันต์\r\n', 'ปรมะเจริญโรจน์\r\n', '123456', 'nuttvanun@tlogical.com', 'Chief Growth Officer\r\n', NULL, 1),
(4, 'T18001', 'น.ส.นภาพร\r\n', 'จูสุวรรณ์\r\n', '123456', 'napaporn@tlogical.com', 'Project Coordinate\r\n', NULL, 0),
(5, 'T19004', 'นายสิรวิชญ์ \r\n', 'กรีพร\r\n', '123456', 'sirawit@tlogical.com', 'Strategic Planner\r\n', NULL, 0),
(6, 'T19006', 'น.ส.รัตติกาล\r\n', 'กันใจมา\r\n', '123456', 'rattikan@tlogical.com', 'Programmer\r\n', NULL, 0),
(7, 'T19007', 'นายทรงสิทธิ์\r\n', 'กองแก้ว\r\n', '123456', 'songsid@tlogical.com', 'Network & Security Engineer\r\n', NULL, 0),
(8, 'T19008', 'นายฟารีด\r\n', 'หมานเหล็บ\r\n', '123456', 'faris@tlogical.com', 'Network & Security Engineer\r\n', NULL, 0),
(9, 'T20002', 'นายฉัตรชัย\r\n', 'บูรณวิเศษกุล\r\n', '123456', 'chatchai@tlogical.com', 'Programmer\r\n', NULL, 0),
(10, 'T20003', 'นาย ธนินท์โชติ \r\n', 'อนันต์มงคลชัย\r\n', '123456', 'taninchote@tlogical.com', 'Creative motion graphic designer\r\n', NULL, 0),
(11, 'T20004', 'นายเจ๊ะอานัส\r\n', 'ม่องพร้า\r\n', '123456', 'jeharnus@tlogical.com', 'Programmer\r\n', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `no_id` int(20) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `to_id` int(20) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `clickdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `task_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(15) NOT NULL,
  `assigned_date` date DEFAULT NULL,
  `tentative_completion` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `status` int(15) NOT NULL DEFAULT 0,
  `project_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `customer` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `project_manager` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `assigned_date`, `tentative_completion`, `deadline`, `status`, `project_name`, `type`, `customer`, `project_manager`, `member_id`, `task_id`) VALUES
(264, '2020-11-10', '2021-01-29', NULL, 0, 'W21', 0, '89', 'sirawit@tlogical.com', 4, NULL),
(265, '2020-11-10', '2020-11-30', NULL, 0, 'W21 | Prepare Infrastructure', 0, '89', 'rojpiti@tlogical.com', 4, NULL),
(266, '2020-11-10', '2020-12-31', NULL, 0, 'The Campus: Coding', 0, '90', 'sirawit@tlogical.com', 4, NULL),
(267, '2020-11-10', '2020-11-11', NULL, 0, 'The Campus | Prepare Infrastructure', 0, '90', 'rojpiti@tlogical.com', 4, NULL),
(268, '2020-11-10', '2020-11-16', NULL, 0, 'The Campus | device sales plan', 0, '90', 'rojpiti@tlogical.com', 4, NULL),
(269, '2020-11-10', '2020-12-15', NULL, 0, 'WFS | Baz#1', 0, '91', 'rojpiti@tlogical.com', 4, NULL),
(270, '2020-11-10', '2020-11-10', NULL, 0, 'CAT | SPO', 0, '92', 'rojpiti@tlogical.com', 4, NULL),
(271, '2020-11-10', '2020-11-30', NULL, 0, 'FNM | IT Fundamental', 0, '93', 'rojpiti@tlogical.com', 4, NULL),
(272, '2020-11-10', '2020-11-13', NULL, 0, 'FNM | IT Fundamental', 0, '93', 'rojpiti@tlogical.com', 4, NULL),
(273, '2020-11-10', '2020-12-23', NULL, 0, 'FNM | Conference System', 0, '93', 'rojpiti@tlogical.com', 4, NULL),
(274, '2020-11-10', '2020-12-11', NULL, 0, 'FNM | Display System', 0, '93', 'rojpiti@tlogical.com', 4, NULL),
(276, '2020-11-10', '2020-11-11', NULL, 0, 'JIN\" Digital\" S\'ignage', 0, '94', 'rojpiti@tlogical.com', 4, NULL),
(277, '2020-11-10', '2020-11-13', NULL, 0, 'ZEN | Online MKT', 0, '95', 'nuttvanun@tlogical.com', 8, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(15) NOT NULL,
  `client` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `project` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `detail` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `upload_file` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `customer_request` varchar(11) DEFAULT NULL,
  `submission` date DEFAULT NULL,
  `member_id` int(15) DEFAULT NULL,
  `assigned_date` date DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT 0,
  `project_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `client`, `project`, `detail`, `subject`, `due_date`, `upload_file`, `type`, `customer_request`, `submission`, `member_id`, `assigned_date`, `status`, `project_id`) VALUES
(413, '95', 'ZEN | Online MKT', 'ขายปาร้าออนไลน์ จัดส่งพัสดุ', 'ขายปาร้าออนไลน์', '2020-11-10', NULL, NULL, '', NULL, 4, '2020-11-10', 0, 277),
(414, '93', 'FNM | Conference System', 'ออกแบบระบบ Conferance', 'ออกแบบระบบ Conferance', '2020-11-10', 'Screen Shot 2563-11-10 at 12.07.50.png', NULL, '', NULL, 4, '2020-11-10', 0, 273),
(416, '89', 'W21', 'ดำเนินการเรียบร้อยแล้ว', 'Proposal', '2020-11-10', NULL, NULL, '', NULL, 5, '2020-11-10', 3, 264),
(417, '90', 'The Campus: Coding', '1. ออกแบบ ER Daigram\r\n2. วางแบบตาม design\r\n3. เขียนโปรแกรม\r\n', 'Coding', '2020-11-10', NULL, NULL, '', NULL, 4, '2020-11-10', 0, 268),
(418, '92', 'CAT | SPO', 'SURVEY', 'SURVEY', '2020-11-11', NULL, NULL, '', NULL, 4, '2020-11-10', 0, 270);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `approved`
--
ALTER TABLE `approved`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `assigned`
--
ALTER TABLE `assigned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assigned_task`
--
ALTER TABLE `assigned_task`
  ADD PRIMARY KEY (`ass_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `feedmes`
--
ALTER TABLE `feedmes`
  ADD PRIMARY KEY (`feed_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`no_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `approved`
--
ALTER TABLE `approved`
  MODIFY `app_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=919;

--
-- AUTO_INCREMENT for table `assigned`
--
ALTER TABLE `assigned`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1387;

--
-- AUTO_INCREMENT for table `assigned_task`
--
ALTER TABLE `assigned_task`
  MODIFY `ass_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `cus_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `feedmes`
--
ALTER TABLE `feedmes`
  MODIFY `feed_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=382;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `no_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1368;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`),
  ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`task_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
