<?php 
   session_start();
   require_once("../../connect.php");
   require_once("../session.php");
   $url = 'genius';
   $section_id ="1";
   $teacher_id=$us;
   $subject_id="2";

   ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>THE CAMPUS พื้นที่แห่งการเรียนรู้ที่ไม่มีวันสิ้นสุด</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <link href="../../assets/css/dashboard.css" rel="stylesheet">
    <link href="../../assets/css/fontawesome/css/all.css" rel="stylesheet">
    <?php  date_default_timezone_set('Asia/Bangkok'); ?>

    <style>
    body {
        font-family: 'Lato', sans-serif;
    }

    .overlay {
        height: 100%;
        width: 0;
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #FFFFFF;
        background-color: #FFFFFF;
        overflow-x: hidden;
        /* transition: 0.5s;*/
    }

    .overlay-content {
        position: relative;
        top: 25%;
        width: 100%;
        text-align: center;
        margin-top: 30px;
    }

    .overlay a {
        padding: 8px;
        text-decoration: none;
        font-size: 36px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .overlay a:hover,
    .overlay a:focus {
        color: #f1f1f1;
    }

    .overlay .closebtn {
        position: absolute;
        top: 20px;
        right: 45px;
        font-size: 60px;
    }

    @media screen and (max-height: 450px) {
        .overlay a {
            font-size: 20px
        }

        .overlay .closebtn {
            font-size: 40px;
            top: 15px;
            right: 35px;
        }
    }

    .result_s {
      height: 15vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
      margin-bottom: 1rem;
    }
    .result_s1 {
      height:25vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
      margin-bottom: 1rem;
    }
    .result_sm
    {
      height:25vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
      margin-bottom: 1rem;
      
    }
    #result {
      height: 52vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
      margin-bottom: 1rem;
    }
    #result1 {
      height: 32vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
      margin-bottom: 1rem;
    }

    #result_mes {
      height: 43vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
    }


    #result_mes1 {
      height: 24vh;
      overflow: scroll;
      overflow-x: hidden;
      font-size: 0.8rem;
    }

    .chat_s {
      max-width: 100%;
      color: #A5C8D9;
      cursor: pointer;
      font-size: 0.9rem;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .chat_s2 {
      max-width: 80%;
      margin-bottom: 5px;
      border: 1px solid #D8D6D6;
      padding: 0.3rem;
      border-radius: 1.3rem;
      padding-left: 1.0rem;
      padding-right: 1.0rem;
      box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.23);
      background-color: #D8D6D6;
      color: #6C6C6C;
      cursor: pointer;
      float: left;
      font-size: 0.9rem;
    }

    .chat_s1 {
      max-width: 80%;
      margin-bottom: 5px;
      border: 1px solid #8AB1C4;
      padding: 0.3rem;
      border-radius: 1.3rem;
      padding-left: 1.0rem;
      padding-right: 1.0rem;
      box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.23);
      background-color: #8AB1C4;
      color: #FFF;
      cursor: pointer;
      float: right;
      font-size: 0.9rem;
    }

    .chat_biard {
      max-width: 85%;
      margin-bottom: 6px;
      border: 1px solid #B1B1B1;
      padding: 0.5rem;
      border-radius: 0.3rem;
      box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.23);
      background-color: #B1B1B1;
      color: #FFF;
    }

    .chat_biard1 {
      max-width: 85%;
      margin-bottom: 6px;
      border: 1px solid #A5C8D9;
      padding: 0.5rem;
      border-radius: 0.3rem;
      box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.23);
      background-color: #A5C8D9;
      color: #FFF;
    }

    .chats strong {
      color: #6d84b4
    }

    .chats p {
      font-size: 14px;
      color: #aaa;
      margin-right: 10px
    }

    .list-group-item.bottom_s {
      border-bottom-right-radius: 0.75rem;
      border-bottom-left-radius: 0.75rem;
    }

    #index6 {
      position: fixed;
      left: 0;
      width: 17vw;
      bottom: 0;
      z-index: 999999;
    }

    .btnwhite {
      border-radius: 1.3rem;
      padding-left: 1rem;
      padding-right: 1rem;
      width: 88px;
    }

    .btnc_white {
      border-radius: 1.3rem;
      padding-left: 1rem;
      padding-right: 1rem;
    }

    .sidepanel {
      width: 0;
      position: absolute;
      z-index: 1;
      height: auto;
      top: 0;
      left: 15px;
      background-color: #FFF;
      overflow-x: hidden;
      /*   transition: 0.5s;
    padding-top: 60px;*/
    }

    .sidepanel a {
      text-decoration: none;
      color: #A5C8D9;
      display: block;
      /*  transition: 0.3s;*/
    }

    .sidepanel a:hover {
      color: #A5C8D9;
    }

    .openbtn {
      font-size: 20px;
      cursor: pointer;
      background-color: #111;
      color: white;
      padding: 10px 15px;
      border: none;
    }

    .openbtn:hover {
      background-color: #444;
    }

    .circle {
      width: 20px;
      height: 20px;
      line-height: 20px;
      border-radius: 50%;
      -moz-border-radius: 50%;
      -webkit-border-radius: 50%;
      text-align: center;
      color: white;
      font-size: 0.7rem;
      text-transform: uppercase;
      /* font-weight: 700; */
      margin: 0 auto 0px;
    }

    .green {
      background-color: #A5C8D9;
    }
    </style>
    <?php   date_default_timezone_set('Asia/Bangkok'); ?>
</head>

<body class="authentication-bg pb-0">
    <div class="auth-fluid">
        <!--Auth fluid left content -->
        <div class="auth-fluid-form-box">
            <div class="h-100">
                <div class="card-body">
                    <!-- Logo -->
                    <div class="auth-brand text-center text-lg-left">
                        <a href="index_demo.php" class="logo-dark"><span><img src="../../assets/images/logo.svg" alt=""
                                    height="80"></span></a>
                        <a href="index_demo.php" class="logo-light"><span><img src="../../assets/images/logo.svg" alt=""
                                    height="80"></span></a>
                    </div>
                    <!-- Logo -->
                    <!--******************************************* DASHBOARD*************************************************-->
                    <div class="row new-row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">DASHBOARD</div>
                                <div class="card-body bg-code">
                                    <div class="row">
                                        <div class="col-sm-4 border-new">
                                            <div>STUDENTS</div>
                                            <div class="new-top">
                                                <samp><i class="fas fa-square-full finish"></i>Online</samp>
                                                <samp><i class="fas fa-square-full unfinish"></i>Offline</samp>
                                                <samp><i class="fas fa-square-full non-active"></i>Unattended</samp>
                                            </div>
                                            <div id="autostd">
                                                <canvas id="pie-chart" width="800" height="450"></canvas>
                                            </div>
                                            <div class="text-right padding-btn"><a href="details_students.php"
                                                    class="btn btn-info btn-rounded">Details</a></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div>QUIZ 1</div>
                                            <div class="new-top">
                                                <samp><i class="fas fa-square-full finish"></i>Finish</samp>
                                                <samp><i class="fas fa-square-full unfinish"></i>Unfinish</samp>
                                                <samp><i class="fas fa-square-full non-active"></i>Non-Active</samp>
                                            </div>
                                            <div id="autoquiz">
                                                <canvas id="myChart" width="800" height="450"></canvas>
                                                <?php include("summary_graph.php");?>
                                                <?php  if( $num_rows8 == 0 ){} else{?>
                                                <div class="text-right padding-btn"><a href="datails_surpisedquiz.php"
                                                        class="btn btn-info btn-rounded">Details</a></div>
                                                <?php } ?>
                                            </div>

                                        </div>
                                        <div class="col-sm-4">
                                            <div>SUMMARY <div id="time"> <?php  echo date("H:i:s");?> </div>
                                            </div>

                                            <div class="new-top">
                                                <div class="progress" id="autolistsummary">
                                                    <?php include("listsummary.php");?>
                                                </div>
                                                <div class="new-top" style="font-size: 0.6rem;">
                                                    <div class="row">
                                                        <div class="col-lg-6"><i class="fas fa-square-full bad"></i>Bad
                                                            (Below 50%)</div>
                                                        <div class="col-lg-6"><i
                                                                class="fas fa-square-full poor"></i>Poor (50-60%)</div>
                                                        <div class="col-lg-6"><i
                                                                class="fas fa-square-full moderate"></i>Moderate
                                                            (60-70%)</div>
                                                        <div class="col-lg-6"><i
                                                                class="fas fa-square-full good"></i>Good (70-80%)</div>
                                                        <div class="col-lg-12" style="padding-left:12px;"><i
                                                                class="fas fa-square-full excellent"></i>Excellent (More
                                                            than 80%)</div>
                                                    </div>
                                                </div>
                                                <div class="bo">Most Incorrect Question</div>
                                                <div id="autotop3">
                                                    <?php  include("select_incorrect.php");?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card-->
                        </div>
                        <!-- end col-->
                    </div>
                    <!--************************************END DASHBOARD**************************************-->
                    <div class="row">
                        <!--************************************ CONFERRENCE TITLE**********************************-->
                        <div class="col-md-5 col-sm-5">
                            <div class="card">
                                <?php include("std2_graph.php");?>
                                <div class="card-header" id="autocheckonloine">
                                    <div style="text-alight: left;  float:left;  "> CONFERRENCE TITLE</div>
                                    <div style="text-align: right; float:right;"> <i class="fa fa-eye"
                                            aria-hidden="true"> <?php echo $checkonline;?></i></div>
                                </div>
                                <div class="card-body bg-code">
                                    <iframe width="260" height="215" src="https://www.youtube.com/embed/bkpVSeThLBI"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                                <!-- end card-body-->
                            </div>
                            <!-- end card-->
                        </div>
                        <!-- end col-->
                        <!--************************************END CONFERRENCE TITLE*******************************-->
                        <!--************************************ QUIZ LISTS******************************************-->
                        <div class="col-md-7 col-sm-7">
                            <div class="card">
                                <div class="card-header">QUIZ LISTS </div>
                                <div class="card-body bg-code">
                                    <!--table--------->
                                    <div class="anyClass" id="autoqlist">
                                        <?php include("quiz_list.php");?>
                                    </div>
                                    <!-- end table---->
                                </div>
                            </div>
                        </div>
                        <!--************************************ END QUIZ LISTS**************************************-->
                    </div>
                </div>
                <!-- end .card-body -->
            </div>
            <!-- end .align-items-center.d-flex.h-100-->
        </div>
        <!-- end auth-fluid-form-box-->
        <!-- Auth fluid right content -->
        <div class="auth-fluid-right text-center">
            <!--****************************  ANNOUNCEMENT BOARD *********************** -->
            <div class="row h-25">
                <div class="col-lg-12 p-2">
                    <!-- Chat-->
                    <div class="card" style="height:100%;">
                        <div class="card-body">
                            <div class="dropdown float-right">
                                <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown"
                                    aria-expanded="false">
                                    <i class="mdi mdi-dots-vertical"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Settings</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                </div>
                            </div>
                            <h4 class="header-title mb-3 code-dashboard">ANNOUNCEMENT BOARD</h4>
                            <div class="chat-conversation" id="comment">
                                <ul> 
                                <li class="list-group-item" style="height:23vh">  
                                <div data-simplebar="init" style="height:180px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper scroll-board"
                                                    style="">
                                                    <div class="simplebar-content" style="padding: 0px;"
                                                        v-for="message in messages"
                                                        v-if="message.board_date == board_date && message.status_show == 1">
                                                        <ul class="conversation-list">
                                                            <li class="clearfix">
                                                                <!----------------------------------->
                                                                <div class="conversation-text1">
                                                                    <div class="ctext-wrap1">

                                                                        <button type="button" class="close"
                                                                            @click.prevent="deleteMessage(message)"
                                                                            style="padding-left: 0.2rem; ">&times;</button>
                                                                        <p style="padding-top: 0.6rem;"> {{message.text}} </p>


                                                                    </div> 
                                                                </div>

                                                                <!----------------------------------->
                                                                <!----------------------------------->

                                                                <!----------------------------------->
                                                            </li>

                                                        </ul>










                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </li>
                                <li class="list-group-item">
                                <form class="needs-validation" novalidate="" name="chat-form" id="chat-form"
                                    @submit.prevent="storeMessage">
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group">

                                                <input type="text" class="form-control chat-input" id="msg"
                                                    placeholder="Enter your text" v-on:keyup="typing"
                                                    v-model="messageText">
                                                <div class="input-group-append">
                                                    <button class="btn btn-info chat-send" type="submit"
                                                        id="btnSend"><img src="../../assets/images/send-button.png"></button>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </li>
                                </ul>
                            </div>
                            <!-- end .chat-conversation-->
                        </div>
                    </div>
                    <!-- end card-->
                </div>
            </div>
            <!--**************************** end ANNOUNCEMENT BOARD *********************** -->
            <!--**************************** LIVE QUESTION ********************************* -->
            <div class="row h-75">
                <!-- start chat users-->
                <div class="col-xl-12 col-lg-12 h-100 p-2">
                    <div class="card" style="height:100%;">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col code-dashboard1"> <h4> LIVE QUESTION </h4> </div>
                                <div class="col">
                                    <ul class="nav nav-tabs nav-bordered">
                                        <li class="nav-item">
                                            <a href="#private" data-toggle="tab" aria-expanded="false"
                                                class="nav-link active py-2">Private</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#group" data-toggle="tab" aria-expanded="true"
                                                class="nav-link py-2">All</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <!-- end nav-->
                            <div class="tab-content">




                                <div class="tab-pane show active p-3" id="private">

                                    <!-- users -->
                                    <div class="row">



                                        <div class="col">

                                            <div id="myNav" class="overlay">    
                                           
                                                <div class="col">
                                                    
                                                    <a href="javascript:void(0)" id="refresh" style="text-align:left;"
                                                        onclick="closeNav()">
                                                        <h4 id="self_id">
                                                        </h4>
                                                    </a>

                                                    <div class="chat-conversation">

                                                        <div id="test">
                                                            <div id="refresh_test">

                                                            </div>
                                                        </div>

                                                       
                                                    </div>
                                              
                                                    

                                                   
                                                </div>

                                           <div id="box"  style="position:sticky; right:auto; width:auto; bottom:0px;"> </div> 
                                           
                                            </div>



                                            <div data-simplebar="init" style="max-height:350px;  overflow: auto;">
                                                <div class="simplebar-wrapper" style="margin: 0px;" id="chatall">
                                                    <div class="simplebar-height-auto-observer-wrapper">
                                                        <div class="simplebar-height-auto-observer"></div>
                                                    </div>
                                                    <div class="simplebar-mask">
                                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                                            <div class="simplebar-content-wrapper"
                                                                style="height: auto; ">
                                                                <div class="simplebar-content" style="padding: 0px;"
                                                                    id="refreshChatList">



                                                                    <!---------------------------------------------------------------- Chat ------------------------------------------------------->



                                                                    <?php  include("select_chatprivate.php"); ?>


                                                                </div>
                                                            </div>
                                                            <!-- End col -->
                                                        </div>








                                                        <!-- end users -->
                                                    </div>
                                                    <!-- end Tab Pane-->
                                                </div>
                                                <!-- end tab content-->
                                            </div>

                                            <!-- end card-body-->
                                        </div>
                                        <!-- end card-->
                                    </div>
                                    <!-- end chat users-->
                                    <!-- end user detail -->

                                </div>
                                <!----------->
                                <div class="tab-pane  p-3" id="group">
                                    <!-- users -->
                                    <div class="row">
                                        <div class="col" id="chatgroup">
                                            <div data-simplebar="init" style="max-height: 350px">
                                                <div class="simplebar-wrapper" style="margin: 0px;">
                                                    <div class="simplebar-height-auto-observer-wrapper">
                                                        <div class="simplebar-height-auto-observer"></div>
                                                    </div>

                                                    <ul class="conversation-list" data-simplebar="init"
                                                        style="max-height: 537px">
                                                        <div class="simplebar-wrapper" style="margin: 0px -15px;">
                                                            <div class="simplebar-height-auto-observer-wrapper">
                                                                <div class="simplebar-height-auto-observer"></div>
                                                            </div>
                                                            <div class="simplebar-mask">
                                                                <div class="simplebar-offset"
                                                                    style="right: 0px; bottom: 0px;">
                                                                    <div class="simplebar-content-wrapper"
                                                                        style="height: 100%; overflow: hidden;  padding-right:1px;   width: 100%; height: 380px; overflow-y: scroll; scrollbar-arrow-color:blue; scrollbar-face-color: #e7e7e7; scrollbar-3dlight-color: #a0a0a0; scrollbar-darkshadow-color:#888888">


                                                                        <?php include("group_chat.php");?>





                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>


                                                    <!-- end Tab Pane-->
                                                </div>
                                                <!-- end tab content-->
                                            </div>
                                            <!-- end card-body-->
                                        </div>
                                        <!-- end card-->
                                    </div>
                                    <!-- end chat users-->
                                    <!-- end user detail -->
                                </div>

                                <!----------->
                            </div>
                            <!--**************************** LIVE QUESTION ********************************* -->

                        </div>
                    </div>
                </div>
             



                <div class="modal" id="myModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body">
                                <input type="hidden" id="quiz_id">
                                <div style="color: #A5C8D9;font-size: 1.8rem;margin-top: 0.5rem;">
                                    ท่านต้องการส่งคำถามใช่หรือไม่</div>
                                <form class="form-inline" style="margin-top: 0.5rem;">
                                    <label style="color:#868686;">กรุณากำหนดระยะเวลาในการตอบคำถาม </label>
                                    <?php
                        $sql2 = "SELECT * FROM surprise_quiz WHERE quiz_id = 1 ";
                        $dbquery2=mysqli_query($conn ,$sql2);
                        $num_rows2=mysqli_num_rows($dbquery2);
                        $row = mysqli_fetch_array($dbquery2,MYSQLI_ASSOC);
                        ?>
                                    <input type="hidden" class="form-control" id="resend_quiz" name="resend_quiz"
                                        name="resend_quiz" value="<?php echo $row['resend_quiz']+1;?>">
                                    <input type="text" class="form-control" id="set_time" name="set_time"
                                        name="set_time" min="1" step="0" maxlength="2">

                                    <label style="color:#868686;"> นาที</label>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="btnsend_time">ตกลง</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal"
                                    style="border-radius: 1.3rem;padding-left: 1rem;padding-right: 1rem;">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>

                





                <!-- end auth-fluid-->
                <!-- bundle -->


                <script src="../../assets/js/firebase-app.js"></script>

                <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
                <script src="../../assets/js/firebase-analytics.js"></script>
                <script src="../../assets/js/firebase-database.js"></script>
                <script>
                // Your web app's Firebase configuration
                // For Firebase JS SDK v7.20.0 and later, measurementId is optional
                var firebaseConfig = {
                    apiKey: "AIzaSyAYWfluUdIn2MjptwE6c95vdYwgc0J26L0",
                    authDomain: "monmai-yribyh.firebaseapp.com",
                    databaseURL: "https://monmai-yribyh.firebaseio.com",
                    projectId: "monmai-yribyh",
                    storageBucket: "monmai-yribyh.appspot.com",
                    messagingSenderId: "948217453613",
                    appId: "1:948217453613:web:a0b31e7c6481636ba1aca4"
                };
                // Initialize Firebase
                firebase.initializeApp(firebaseConfig);
                firebase.analytics();
                </script>





                <script src="../../assets/js/tlo.js"></script>
                <script src="../../assets/js/app.js"></script>
                <script src="../../assets/js/sweetalert2.js"></script>
                <script src="../../assets/js/vue.js"></script>
                <script src="../../assets/js/Chart.min.js"></script>
                <script src="../../assets/js/chartjs-plugin-labels.js"></script>

                <script>
                function logoutFn() {

                    Swal.fire({
                        title: 'Do you want to leave THE CAMPUS ?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.isConfirmed) {

                            location.replace("../../logout.php");



                        }
                    })

                }



                $(document).ready(function() {
                    $(document).on('click', '#openChat', function() {
                        var self_id = 0;
                        var self_id = $(this).attr("data-id");
                        var username = $(this).attr("data-name");
                        $("#self_id").val(self_id);

                        $.ajax({
                            url: "pconversation.php",
                            method: "GET",
                            data: {
                                self_id: self_id
                            },
                            success: function(data) {

                                document.getElementById("myNav").style.width = "100%";

                                document.getElementById("self_id").innerHTML = "< " +
                                    username;
                                $('#test').html(data);

                            }
                        });




                        $.ajax({
                            url: "box_send.php",
                            method: "POST",
                            data: {
                                self_id: self_id
                            },
                            success: function(data) {


                                $('#box').html(data);

                            }
                        });







                    });






                });




                /*    $(function(){
                        $('#test').load('pconversation.php?id=self_id'); //ดึงข้อมูลจากไฟล์อื่นมาแสดงใน div id=test โดยไม่ต้องเปลี่ยนไปหน้าอื่น
               
                         }); */


                $(document).ready(function() {
                    $(document).on('click', '#submit', function() {



                        if ($("#msgtext").val() != "") {

                            $.ajax({
                                url: "send_pchat.php",
                                method: "POST",
                                data: {
                                    msg: $('#msgtext').val(),
                                    sender: $('#sender').val(),
                                },

                                success: function(data) {

                                    $('#msgtext').val('');

                                }
                            });

                        }

                    });

                });


                function closeNav() {


                    document.getElementById("myNav").style.width = "0%";
                    $('#refreshChatList').load('refreshselect_chatprivate.php').show();



                }
                </script>

                <script>
                var section_id = '<?php echo $section_id;?>';
                var teacher_id = '<?php echo $teacher_id;?>';
                var t_name = '<?php echo $t_name;?>';
                var subject_id = '<?php echo $subject_id;?>';
                var board_date = '<?php echo date("Y-m-d");?>';
                var board_time = '<?php echo date("H:i");?>';
                var board_timestamp = '<?php echo date("Y-m-d")."/".date("H:i:s");?>';
                var status_show = '<?php echo "1";?>';


                var admin = '<?php echo "teacher";?>';
                var adminlogin = '<?php echo $adminlogin;?>';
                var sender = '<?php echo "teacher";?>';
                var chat_date = '<?php echo date("Y-m-d");?>';
                var chat_time = '<?php echo date("H:i");?>';
                var status_read = '<?php echo "1" ;?>';
                var chatname = chatname;
                </script>

                <!--Firebase--------------------------------------------------------------------------------------------------------------------->

                <!-- The core Firebase JS SDK is always required and must be listed first -->


                <script>
                $(document).ready(function() {


                    $(".results").click(function() {
                        var quiz_id = $(this).attr("data-id");
                        var quiz_time_all = $(this).attr("data-ida");
                        $("#quiz_id").val(quiz_id);
                        $("#set_time").val(quiz_time_all);
                        $('#myModal').modal('show');
                    });


                });
                </script>
                <script>
                $(document).ready(function() {
                    $("#btnsend_time").click(function() {

                        if ($("#set_time").val() == "") {
                            $("#set_time").css("border", "1px solid red");
                            return false;
                        }
                        if ($("#set_time").val() != "") {
                            var resend_quiz = $("#resend_quiz").val();
                            var quiz_id = $("#quiz_id").val();
                            var set_time = $("#set_time").val();
                            //console.log(quiz_id);
                            // console.log(set_time);
                            //console.log(resend_quiz);
                            $.ajax({
                                type: 'post',
                                url: 'sendbox.php',
                                data: {
                                    quiz_id: quiz_id,
                                    set_time: set_time,
                                    resend_quiz: resend_quiz
                                },
                                success: function(response) {
                                    // alert(response);
                                    $('#myModal').modal('hide');
                                    $("#set_time").val('');
                                    $("#quiz_id").val('');
                                    $("#resend_quiz").val('');
                                }
                            });
                        }
                        return false;
                    });
                });
                </script>





                <script src="../../assets/js/boardcast.js"></script>
                <!--<script src="../../assets/js/chatgroup.js"></script>-->




                <script type="text/javascript">
                $(document).ready(function() {
                    var myVar1 = setInterval(setautoRefresh_div1, 3000000);

                    function setautoRefresh_div1() {
                        var id = $("#self_id").val();
                        var url = 'pconversation.php?self_id=' + id;
                        $('#refresh_test').load(url).show();
                        $('#autostd').load('std_graph.php').show();
                        $('#autoquiz').load('quiz_graph.php').show();
                        $('#autolistsummary').load('listsummary.php').show();
                        $('#autotop3').load('select_incorrect.php').show();
                        $('#autocheckonloine').load('checkonline.php').show();
                        $('#autoqlist').load('quiz_list.php').show();
                        $('#time').load('timerefresh.php').show();
                        $('#refreshChatList').load('refreshselect_chatprivate.php').show();
                        $('#chat_group').load('group_chat.php').show();




                    }








                });
                </script>



                <?php  include("std2_graph.php");?>
                <script>
                var canvas = document.getElementById('pie-chart');
                new Chart(canvas, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [<?php echo number_format("$online",2); ?>, <?php echo number_format("$offline",2);?>,
                                <?php  echo number_format("$Unattended",2);?>
                            ],
                            backgroundColor: ['#5FA55A', '#F6D51F', '#FA5457']
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: true,

                        plugins: {

                            labels: {
                                label: 'datasets',
                                render: 'percentage',
                                fontColor: ['#FFFFFF', '#FFFFFF', '#FFFFFF'],
                                precision: 1
                            },
                        },
                        tooltips: {
                            enabled: false,
                        },
                    }
                });
                </script>
                <?php 
                            
                            
                          include("quiz_graph2.php");
                            
                            
                          

                            ?>
                <script>
                var canvas = document.getElementById('myChart');
                new Chart(canvas, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [<?php echo floor($finish); ?>, <?php  echo floor($unfinish) ;?>,
                                <?php echo floor($nonactive);?>
                            ],
                            backgroundColor: ['#5FA55A', '#F6D51F', '#FA5457']
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: true,
                        plugins: {
                            labels: {
                                render: 'percentage',
                                fontColor: ['#FFFFFF', '#FFFFFF', '#FFFFFF'],
                                precision: 1
                            },
                        },
                        tooltips: {
                            enabled: false,
                        },
                    }
                });
                </script>






</body>

</html>