<?php 
session_start();
include("../connect.php");
include("../session.php");

$in_type="3";
$sub_in_type="2";


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <?php  include ('../template/title.php');?>
    <link href="../assets/css/fontawesome/css/all.min.css" rel="stylesheet">
    <link href="../assets/css/app-tlo.css" rel="stylesheet" type="text/css" id="light-style" />
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/app-tlo.min.css" rel="stylesheet" type="text/css" id="dark-style" />

    <link href="../assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script src="../assets/js/ckeditor/ckeditor.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script type="text/javascript">
    $(function() {
        var removeLink =
            ' <a class="remove" href="insert_activity.php" onclick="$(this).parent().slideUp(function(){ $(this).remove() }); return false"><button  class="btn btn-danger" > ลบ</button></a>';
        $('a.add').relCopy({
            append: removeLink
        });
    });
    </script>

</head>

<body class="loading"
    data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false}'>
    <div class="wrapper">

        <div class="content">

            <!-- Topbar Start -->

            <!-- end Topbar -->
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->

                <div class="row show_higth">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="row no-gutters align-items-center">
                                <div class="col-md-3">
                                    <div class="card-body card-home">
                                        <div class="page-title-box">
                                            <a href="#" id="check" class="dropdown-item">
                                                <h4 class="page-title"><i class="fas fa-angle-left"></i>
                                                    เอกสารบันทึกกิจกรรม PLC</h4>
                                            </a>
                                            <hr>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <form class="imgForm" id="insertplc" name="fform" action="insert_plc.php" method="post"
                        enctype="multipart/form-data">

                        <div class="row">
                            <div class=" col-lg-6">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group ">
                                                <input type="text" class="textInput" placeholder="ชื่อกลุ่ม"
                                                    id="group_plc" name="group_plc">
                                                <input type="hidden" class="textInput" value='<?php echo $us; ?>'
                                                    name="teacher_id">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <div class="form-group ">

                                                <input type="text" class="textInput" placeholder="ชื่อกิจกรรม"
                                                    id="name_activity" name="name_activity">


                                            </div>




                                        </div>


                                    </div>



                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-6">

                                            <div class="form-group ">

                                                <label> ครั้งที่</label>
                                                <input type="number" min=1 class="textInput" id="no_time"
                                                    name="no_time">


                                            </div>




                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group ">

                                                <label> วัน/เดือน/ปี</label>

                                                <input type="date" class="textInput" id="date_plc" name="date_plc">


                                            </div>




                                        </div>


                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-6">

                                            <div class="form-group ">

                                                <label> เริ่มเวลา</label>
                                                <input type="time" class="textInput" id="starttime_plc"
                                                    name="starttime_plc">


                                            </div>




                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group ">

                                                <label> สิ้นสุดเวลา</label>

                                                <input type="time" class="textInput" id="endtime_plc"
                                                    name="endtime_plc">


                                            </div>




                                        </div>


                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <div class="form-group ">
                                                <label>รายชื่อผู้เข้าร่วมกิจกรรม</label>




                                                <p class="clone"> <input type="text" name="hobby[]" class='textInput' />
                                                </p>

                                                <p><a href="insert_activity.php" class="add" rel=".clone"><button
                                                            class="btn btn-success"> เพิ่ม</button> </a></p>





                                            </div>




                                        </div>


                                    </div>


                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <div class="form-group ">

                                                <label>ภาพกิจกรรม</label>

                                                <div class="file-loading">
                                                    <input id="input-b6" name="pic[]" type="file" multiple>
                                                </div>


                                               <!-- <div id='Grouppic'>
                                                    <div id="pic1">




                                                        <input class="textInput" type="file" id='pic1' name="pic1" />
                                                        <input class="textInput" type="hidden" id='sum' name="sum[]"
                                                            value="1">
                                                    </div>
                                                </div>
                                                <input type='button' value=' เพิ่มใหม่ ' class="btn btn-success"
                                                    id='addpic'>
                                                <input type='button' value=' ลบทิ้ง ' class="btn btn-danger"
                                                    id='removepic'>  -->



                                            </div>




                                        </div>


                                    </div>


                                </div>

                            </div>





                            <div class="col-xl-6 col-lg-6">
                                <div class="card">
                                    <div class="row ">
                                        <div class="col-md-12">

                                            <div class="form-group ">

                                                <label> ประเด็นปัญหา/สิ่งที่ต้องการพัฒนา</label>

                                                <div class="form-group ">

                                                    <textarea name="problem_plc" id="problem_plc" class="textInput"></textarea>
                                                </div>





                                            </div>




                                        </div>


                                    </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">


                                        </div>


                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <label> สาเหตุของปัญหา</label>

                                            <div class="form-group ">

                                                <textarea name="cause_plc" id="cause_plc" class="textInput"></textarea>
                                            </div>


                                        </div>


                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <label> หลักการที่นำมาใช้</label>

                                            <div class="form-group ">

                                                <textarea name="principle" id="principle" class="textInput"></textarea>
                                            </div>


                                        </div>


                                    </div>


                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <label> กิจกรรมที่ทำหรือแนวทางการแก้ปัญหา/พัฒนา</label>

                                            <div class="form-group ">

                                                <textarea name="glp_plc" id="glp_plc" class="textInput"></textarea>
                                            </div>


                                        </div>


                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <label> นวัตกรรมที่เกิด</label>

                                            <div class="form-group ">

                                                <textarea name="innovationb" id="innovationb"  class="textInput"></textarea>
                                            </div>


                                        </div>


                                    </div>

                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <label> การนำนวัตกรรมไปใช้</label>

                                            <div class="form-group ">

                                                <textarea name="innovationu" id="innovationu" class="textInput"></textarea>
                                            </div>


                                        </div>


                                    </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">

                                            <label> ประเด็นการประชุมครั้งถัดไป</label>
                                            <div class="form-group ">
                                                <textarea name="meeting"  id="meeting" class="textInput"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <button type="button" id="btn_insert"
                                                    class="btn btn-success btn-rounded">บันทึก</button>
                                            </div>
                                        </div>
                                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <!--- end rigth-->
    </div>
    <!-- end page title -->
    </div> <!-- container -->

    </div> <!-- content -->
    <?php  include ('../template/footer.php');?>
    </div>
    </div>



    <script src="../assets/js/vendor.min.js"></script>
    <script src="../assets/js/app.min.js"></script>
    <script type="text/javascript" src="../assets/js/reCopy.js"></script>

    <script src="../assets/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="../assets/js/fileinput.js" type="text/javascript"></script>
    <script src="../assets/js/locales/fr.js" type="text/javascript"></script>
    <script src="../assets/js/locales/es.js" type="text/javascript"></script>

    <script>
$(document).ready(function() {
    $("#input-b6").fileinput({
        showUpload: true,
        dropZoneEnabled: false,
        maxFileCount: 10,
        mainClass: "input-group-lg",
        allowedFileExtensions: ["jpg", "png", "gif", "tiff"]
    });
});
</script>

    <script>
    CKEDITOR.replace('problem_plc');
    CKEDITOR.replace('cause_plc');
    CKEDITOR.replace('principle');
    CKEDITOR.replace('glp_plc');
    CKEDITOR.replace('innovationb');
    CKEDITOR.replace('innovationu');
    CKEDITOR.replace('meeting');
    </script>

    <script>
    //delete===================================================================================================================================== 
    $(document).on('click', '#check', function() {


        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'คุณยังไม่ได้กดบันทึก',
            text: "ต้องการบันทึกก่อนออกจากหน้านี้หรือไม่ ?",
            icon: 'warning',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'บันทึก',
            cancelButtonText: 'ไม่บันทึก',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {

                //ถ้า confirm


                if ($("#group_plc").val() == "" && $("#name_activity").val() == "") {

                    $("#group_plc").css("border", "1px solid red");
                    $("#name_activity").css("border", "1px solid red");

                    return false;
                }
                if ($("#group_plc").val() == "") {
                    $("#group_plc").css("border", "1px solid #f70404");
                    return false;
                }
                if ($("#name_activity").val() == "") {
                    $("#name_activity").css("border", "1px solid #f70404");
                    return false;
                }



                if ($("#name_activity").val() != "" &&
                    $("#group_plc").val() != "") {


                    $("#insertplc").submit();
                    return false;


                }



            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

                window.location = 'activity_list.php';
                //ถ้า cancel
            }
        })






    });
    </script>


    <script type="text/javascript">
    //เช็คค่าว่าง insert=====================================================================================================================================   

    $(document).ready(function() {

        $('#btn_insert').click(function() {



                $("#insertplc").submit();
                return false;



        });







    });




    //เพิ่ม อัพโหลดภาพ=====================================================================================================================================   




    $(document).ready(function() {
        var counter = 2;

        $("#addpic").click(function() {


            var pic = $(document.createElement('div'))
                .attr("id", 'pic' + counter);

            pic.after().html(
                '<input class="textInput" type="file"   name="pic' + counter + '" id="pic' +
                counter +
                '" value="" > <input class="textInput" type="hidden" id="sum" name="sum[]" value="' +
                counter + '" >');

            pic.appendTo("#Grouppic");
            counter++;
        });

        $("#removepic").click(function() {
            if (counter == 2) {
                alert("คุณได้ลบกล่องข้อความหมดแล้ว");
                return false;
            }
            counter--;
            $("#pic" + counter).remove();
        });


    });
    </script>











    <script type="text/javascript">
    function isThaichar(str, obj) {
        var orgi_text = "ๅภถุึคตจขชๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝ๑๒๓๔ู฿๕๖๗๘๙๐ฎฑธํ๊ณฯญฐฅฤฆฏโฌ็๋ษศซฉฮฺ์ฒฬฦ";
        var str_length = str.length;
        var str_length_end = str_length - 1;
        var isThai = true;
        var Char_At = "";
        for (i = 0; i < str_length; i++) {
            Char_At = str.charAt(i);
            if (orgi_text.indexOf(Char_At) == -1) {
                isThai = false;
            }
        }
        if (str_length >= 1) {
            if (isThai == false) {
                obj.value = str.substr(0, str_length_end);
            }
        }
        return isThai; // ถ้าเป็น true แสดงว่าเป็นภาษาไทยทั้งหมด
    }
    </script>
</body>

</html>