<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap3.4.1.min.css">
    <title>Completion</title>
    <head>
<?php include('template/leftbar.php'); ?>
<?php include('template/topbar.php'); ?>
<style>
    td.ng {
        color: red;
    }
</style>
</head>
<body>
<div class="container" style="padding-top: 5rem ; padding-left: 7rem;">
    <h3>COMPLETION:</h3>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#">MYTASK</a></li>
        <li><a href="complet2.php">FOLLOW UP</a></li>
    </ul>
        <div class="col-xl-10">
            <table class="table table-centered mb-0">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Client</th>
                        <th>Subject</th>
                        <th>Assigned Date</th>
                        <th>Due Date</th>
                        <th>SUBMISSION</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <?php
                 $perpage = 10;
                 if (isset($_GET['page'])) {
                     $page = $_GET['page'];
                 } else {
                     $page = 1;
                 }
                 $start = ($page - 1) * $perpage;
 
                    $query = "SELECT * FROM assigned_task INNER JOIN task on task.task_id = assigned_task.task_id where assigned_task.member_id = '".$us."' AND NOT status = '1,2' group by assigned_task.task_id desc limit {$start},{$perpage} ";
                    $query_run = mysqli_query($conn, $query);
                    $m = 0;
                while ($row = mysqli_fetch_array($query_run, MYSQLI_ASSOC)) {
                    $datenow = date('Y-m-d');  //วันที่ปัจจุบัน
                    $m++;
                    $status = '';

                    $date1 =  explode(' ', $row['assigned_date']);
                    $start_time1  = $date1[0];
                    $datee1 = explode('-', $start_time1);
                    $year1=  $datee1[0];
                    $month1 = $datee1[1];
                    $day1 = $datee1[2];
                    $strdate = $day1 . '/' . $month1 . '/' . $year1;

                    $date =  explode(' ', $row['due_date']);
                    $start_time  = $date[0];
                    $datee = explode('-', $start_time);
                    $year =  $datee[0];
                    $month = $datee[1];
                    $day = $datee[2];
                    $enddate = $day . '/' . $month . '/' . $year;
                ?>
                    <tbody>
                        <tr>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $m ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$m; </td> ';
                            }
                            echo $m;
                            ?>
                            <?php $sql3 = "SELECT customer_name from customer where cus_id = '" . $row['client'] . "' ";
                            $query3 = mysqli_query($conn, $sql3);
                            while ($rowc = mysqli_fetch_array($query3, MYSQLI_ASSOC)) { ?>

                                <?php
                                if ($row['due_date'] >= $datenow) {
                                    echo '<td $client ;  </td> ';
                                } elseif ($row['due_date'] < $datenow) {
                                    echo '<td class="ng".$client; </td> ';
                                }
                                echo $rowc['customer_name'];
                                ?>
                            <?php } ?>

                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $subject ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$subject; </td> ';
                            }
                            echo $row['subject'];
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $assigned_date ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$assigned_date; </td> ';
                            }
                            echo $strdate;
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $due_date ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$due_date; </td> ';
                            }
                            echo  $enddate;
                            ?>
                            <?php
                            if ($row['due_date'] >= $datenow) {
                                echo '<td $due_date ;  </td> ';
                            } elseif ($row['due_date'] < $datenow) {
                                echo '<td class="ng".$due_date; </td> ';
                            }
                            echo $enddate;
                            ?>
                            
                    <?php 
                            if ( $row["status"] == '0' &&  $row['due_date']>=$datenow)
                            {   
                               $pathx = "assets/images/power.png";
                            echo ' <td> On <img src="'.$pathx.'" width="28"pix" ></td>';  
                            }
                            elseif ($row["status"] == '0' && $row['due_date']<$datenow) 
                            {   
                               $pathx = "assets/images/alert1.jpg";
                            echo ' <td class="ng">Overdue<img src="'.$pathx.'" width="30pix"; > </td> ';
                            } 
                            if  ($row["status"]== "3" &&  $row['due_date']<$datenow ) {
                                $pathx = "assets/images/loading.png";
                            echo ' <td class="ng" >Waiting<img src="'.$pathx.'" width="30pix"; > </td> ';
                            }
                            elseif ($row["status"] == "3" && $row['due_date']>=$datenow) { 
                                $pathx = "assets/images/loading.png";
                            echo ' <td>Waiting<img src="'.$pathx.'" width="30pix"; > </td> ';
                            }
                            elseif ($row["status"] == "4") {
                            $pathx = "assets/images/greencheck.png";
                            echo '<td >Completed<img src="' . $pathx . '" width="20pix"; > </td> ';
                            }
                            elseif ($row["status"] == "5") {
                                $pathx = "assets/images/notification-bell.png";
                                echo '<td>Feedback<img src="' . $pathx . '" width="20pix"; > </td> ';
                       
                            }
                            ?>
                        </tr>
                        </tbody>
                    <?php } ?>
                    <tr>
   
   <td>
   </td>
 
   <td>
       <?php
       $sql2 = "SELECT * FROM assigned_task INNER JOIN task on task.task_id = assigned_task.task_id where assigned_task.member_id = '".$us."' AND NOT status = '1,2' group by assigned_task.task_id desc ";
       $query2 = mysqli_query($conn, $sql2);
       $total_record = mysqli_num_rows($query2);
       $total_page = ceil($total_record / $perpage);
       if ($total_record > 10) {
       ?>
           <nav aria-label="Page navigation exaple mt-5">
               <ul class="pagination justify-content-center">
                   <li class="page-item">
                       <a class="page-link" href="completion.php?page=1"> Previous</a>
                   </li>
                   <?php for ($i = 1; $i <= $total_page; $i++) { ?>
                       <li class="page-item <?php if ($page == $i) {
                                                   echo 'active';
                                               } ?>">
                           <a class="page-link" href="completion.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                       </li>
                   <?php } ?>
                   <li class="page-item">
                       <a class="page-link" href="completion.php?page=<?php echo $total_page; ?>">
                           Next
                       </a>
                   </li>
               </ul>
           </nav>
       <?php  }   ?>
   </td>
 
</tr>
</div>
</div>

                    </body>
</html>