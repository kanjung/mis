<?php
session_start();
require_once("connect.php");
require_once("session.php");

$task_id = $_GET['task_id'];
$query = "SELECT * FROM task where task_id  = '" . $task_id . "' ";
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

$date =  explode(' ', $row['due_date']);
$start_time  = $date[0];
$datee = explode('-', $start_time);
$year =  $datee[0];
$month = $datee[1];
$day = $datee[2];

$client = $row['client'];
$project = $row['project'];
$subject = $row['subject'];
$detail = $row['detail'];
$upload_file = $row['upload_file'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="shortcut icon" href="assets/images/favicon.ico">
    <title>Edit Task</title>
    <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="chosen/chosen.css">
    <link href="css/gijgo.min.css" rel="stylesheet" type="text/css" />
    </head>
<body>
    <?php   include('template/leftbar.php');
            include('template/topbar.php');  ?>
    <div class="container">
    <div class="col-xl-12" style="padding-top: 5rem ;">
        <H3>Edit TASK</H3>
        <form action="save_task.php?task_id=<?php echo $_GET['task_id']; ?>" method="POST" id="frmsave" enctype="multipart/form-data">
            
            <div class="form-group form-check">
                client :
                    <?php
                    $sql = "SELECT * FROM customer where cus_id = '" . $client . "'";
                    $query = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                        $customer = $row["customer_name"];
                        $cus_id = $row['cus_id'];
                    ?>
                 <input class="form-control" type="text" id="client1" name="client1" readonly placeholder="client" value=" <?php echo $customer; ?> ">
                 <input class="form-control" type="hidden" id="client" name="client" readonly  value=" <?php echo $row['cus_id']; ?>">
                    <?php } ?>
            </div>

            <div class="form-group form-check">
                Project :
            <?php
                $sql1 = "SELECT * FROM projects where customer = '" . $cus_id . "' ";
                $query1 = mysqli_query($conn, $sql1);
                while ($row1 = mysqli_fetch_array($query1, MYSQLI_ASSOC)) {
                    $project_id = $row1['project_id'];
                }  ?>

                <input  placeholder="Project" type="text" id="project" name="project" readonly class="form-control" value="<?php echo $project; ?>">
            </div>

            <div class="form-group form-check">
                Due Date :
                <input value="<?php echo $day . '/' . $month . '/' . $year; ?>" placeholder="Due Date" type="text" id="due_date1" name="due_date1" class="form-control" autocomplete="off" disabled>
                <input type="hidden" id="due_date" name="due_date"> 

            </div>

            <div class="form-group form-check">
                Subject :
                <input value="<?php echo $subject; ?>" type="text" name="subject" id="subject" class="form-control  " placeholder="Subject">
            </div>

            <div class="form-group form-check">
                Detail :
                <textarea name="detail" id="detail" class="form-control" placeholder="Detail" rows="4"><?php echo $detail; ?></textarea>
            </div>

            <div class="form-group form-check">
                Assigned :
                <select data-placeholder="assigned" class="chosen-select1 form-control" multiple tabindex="6" id="assigned" name="assigned[]">
                    <?php
                    $strSql1 = "SELECT * FROM assigned_task WHERE  task_id  = '" . $task_id . "' ";
                    $query7 = mysqli_query($conn, $strSql1);
                    $num1 = mysqli_num_rows($query7);
                    $resultArray7 = array();
                    
                    for ($i = 0; $i < $num1; $i++) {
                        $result7 = mysqli_fetch_array($query7);
                        array_push($resultArray7, $result7);
                    }
                    foreach ($resultArray7 as $result7) {
                    ?>
                        <option selected="selected" value="<?php echo $result7["assigned"]; ?>"><?php echo $result7["assigned"]; ?></option>
                    <?php } ?>

                </select>
            </div>

            <div class="form-group form-check">
                Approved :
                <select data-placeholder="approved" class="chosen-select2 form-control" multiple tabindex="6" id="approved" name="approved[]" placeholder="approved">
                    <?php
                    $strSql1 = "SELECT * FROM approved WHERE  task_id  = '".$task_id."' ";
                    $query3 = mysqli_query($conn, $strSql1);
                    $num = mysqli_num_rows($query3);
                    $resultArray1 = array();
                    $test = "";
                    for ($i = 0; $i < $num; $i++) {
                        $result3 = mysqli_fetch_array($query3,MYSQLI_ASSOC);
                       // array_push($resultArray1, $result3);
                        $resultArray1[] =   $result3["approved"];           
                    ?>
                        <option selected="selected" value="<?php echo $result3["approved"]; ?>"><?php echo $result3["approved"]; ?></option>
                    <?php
                    }
                    $test = implode(',',$resultArray1);
                       $sql3 = "SELECT * FROM member where  email_address  NOT IN('".str_replace(",", "','", $test)."') ";
                        $query5 = mysqli_query($conn, $sql3);  ?>
                    <?php 
                    while ($row3 = mysqli_fetch_array($query5, MYSQLI_ASSOC)) {
                    ?>
                        <option value="<?php echo $row3["email_address"]; ?>"><?php echo $row3["email_address"]; ?></option>
                    <?php }  ?>
                </select>
            </div>
            
            <div class="form-group form-check">
                Upload File :
                <input value="<?php echo $upload_file;?>" type="file" name="upload_file" id="upload_file" class="form-control" placeholder="upload_file" enctype="multipart/form-data">
            </div>
            <input type="hidden" class="form-control" id="member_id" name="member_id" value="<?php echo $us; ?>">
            <button type="button" class="btn btn-dark btn-rounded" id="addbtn" style="border-radius: 2rem;padding-left: 1.5rem;padding-right: 1.5rem;" value="upload">Save</button>
            <a href="view_task.php"> <button type="button" class="btn btn-dark btn-rounded" style="border-radius: 2rem;padding-left: 1.5rem;padding-right: 1.5rem;">Cancel</button></a>
        </form>
    </div>
</div>

<!-------------------------------------------script---------------------------------------------------------->
<script src="js/fileinput.js" type="text/javascript"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="js/gijgo.min.js" type="text/javascript"></script>
<script>
    $(".chosen-select2").chosen()

    $(".chosen-select1").chosen()

    jQuery(document).ready(function() {
        jQuery(".chosen-select").chosen();
    });

    $(document).ready(function() {
        $("#upload_file").fileinput({
            showUpload: true,
            dropZoneEnabled: false,
            maxFileCount: 10,
            mainClass: "input-group-lg"
        });
    });

    $(document).ready(function() {
        $('#addbtn').click(function() {
            if ( $("#project").val() == "" && $("#due_date1").val() == "" && $("#subject").val() == "" &&
                $("#detail").val() == "") 
                {
                $("#cilent").css("border", "1px solid red");
                $("#project").css("border", "1px solid red");
                $("#due_date1").css("border", "1px solid red");
                $("#subject").css("border", "1px solid red");
                $("#detail").css("border", "1px solid red");
                $("#assigned_chosen").css("border", "1px solid red");
                $("#approved_chosen").css("border", "1px solid red");
                return false;
            }
            if ($("#cilent").val() == "") {
                $("#cilent").css("border", "1px solid red");
                return false;
            }
            if ($("#project").val() == "") {
                $("#project").css("border", "1px solid red");
                return false;
            }
            if ($("#due_date1").val() == "") {

                $("#due_date1").css("border", "1px solid red");
                return false;
            }
            if($("#due_date1").val() != "") 
            {
                var te = $("#due_date1").val();
                $("#due_date").val(te);
              //  return false;
            }
            if ($("#subject").val() == "") {

                $("#subject").css("border", "1px solid red");
                return false;
            }
            if ($("#detail").val() == "") {

                $("#detail").css("border", "1px solid red");
                return false;
            }
            if ($("#assigned").chosen().val() == null) {

                $("#assigned_chosen").css("border", "1px solid red");
                return false;
            }
            if ($("#approved").val() == null) {

                $("#approved_chosen").css("border", "1px solid red");
                return false;
            }

            $("#frmsave").submit();
            return false;

        })
    });

    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#due_date1').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            minDate: today
        });

    /*      $(".client").change(function() {
              var client = $(".client").val();
              $.ajax({
                  type: "POST",
                  url: "showaddtask.php",
                  data: {
                      isclient: client
                  },
                  success: function(data) {
                      $('#projects').html(data);

                  }
              });
          });   */
          
</script>
</body>
</html>