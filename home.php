<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/simple-donut.css" type="text/css">
    <title>Home</title>
    <style>
        .card-body {
            color: white;
            background-color: white;
            background-image: linear-gradient(white, #001433);
        }
        .pie-wrapper .half-circle1 {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            border: 0.1em solid #c41919;
            border-radius: 50%;
            clip: rect(0em, 0.5em, 1em, 0em);
        }
    </style>
</head>
<body>
    <?php include('template/leftbar.php'); ?>
    <?php include('template/topbar.php'); ?>
    <div class="container" style="padding-top:5rem; padding-left:5rem; ">
        <H2>MY PROJECTS</H2>
        <div class="row">
            <?php
            $sql_m = "SELECT * FROM projects INNER JOIN 
            assigned on projects.project_id = assigned.project_id  where assigned.member_id = '".$us."' or projects.member_id = '".$us."' group by projects.project_id ";
            $query_m = mysqli_query($conn, $sql_m);
            $num5 = mysqli_num_rows($query_m);
            if ($num5 < 1) {
                echo "no data";
            }
            $n = 0;
            while ($row_m = mysqli_fetch_array($query_m, MYSQLI_ASSOC)) {
                $project_id = $row_m["project_id"];

                $n++;

                $sql = "SELECT count(task_id) FROM task where project_id = '" . $project_id . "'  AND  customer_request != '1'";
                $query3 = mysqli_query($conn, $sql);
                $row = mysqli_fetch_array($query3, MYSQLI_ASSOC);
                $num1 = $row['count(task_id)'];
                
                $sql1 = "SELECT count(status) FROM task where project_id = '" . $project_id . "' AND status='4' AND customer_request != '1' ";
                $result1 = mysqli_query($conn, $sql1);
                $row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
                $num = $row1['count(status)'];
                
                if ($num1 != 0) {
                    $show = @(round($num * 100) / $num1);
                } else {
                    $show = '0';
                }

                if ($show == '100') {
                    $query = "UPDATE projects SET
                    status = '4' 
                    where project_id = '" . $project_id . "'  ";
                    $query4 = mysqli_query($conn, $query);
                }

                $date =  explode(' ', $row_m['tentative_completion']);
                $start_time  = $date[0];
                $datee = explode('-', $start_time);
                $year =  $datee[0];
                $month = $datee[1];
                $day = $datee[2];
                $enddate = $day . '/' . $month . '/' . $year;
            ?>
                <?php if ($row_m['type'] == '1') { ?>
                    <div class="col-3">
                        <div class="container" style="padding-top: 2rem; padding-left:0rem; ">
                            <a href="detail_project.php?project_id=<?php echo $row_m["project_id"]; ?>">
                                <div class="card-body rounded-lg">
                                    <div id="specificChart<?php echo $n; ?>" class="donut-size">
                                        <div class="pie-wrapper">
                                            <span class="label">
                                                <span class="num"><?php echo $show; ?></span><span class="smaller">%</span>
                                            </span>
                                            <div class="pie">
                                                <div class="left-side half-circle1"></div>
                                                <div class="right-side half-circle1"></div>
                                            </div>
                                            <div class="shadow"></div>
                                        </div>
                                        <div class="row align-items-center pa">

                                            <div class="col-8">
                                                <h6 class="text-left" style="font-size:20px">
                                                    <?php $sql6 = "SELECT customer_name from customer where cus_id = '" . $row_m['customer'] . "' ";
                                                    $query6 = mysqli_query($conn, $sql6);
                                                    while ($rowc = mysqli_fetch_array($query6, MYSQLI_ASSOC)) { ?>
                                                        <?php echo $rowc['customer_name']; ?>
                                                    <?php } ?></h6>
                                                <h6 class="text-left" style="font-size:20px"><?php echo $row_m["project_name"]; ?></h6>
                                                <h6 class="text-left" style="font-size:11px">TentativeCompletion:</h6>
                                                <h6 class="text-left" style="font-size:11px"><?php echo $enddate; ?></h6>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                <?php } else { ?>

                    <div class="col-3">
                        <div class="container" style="padding-top: 2rem; padding-left:0rem; ">
                            <a href="detail_project.php?project_id=<?php echo $row_m["project_id"]; ?>">
                                <div class="card-body pa  rounded-lg">

                                    <div id="specificChart<?php echo $n; ?>" class="donut-size">
                                        <div class="pie-wrapper">
                                            <span class="label">
                                                <span class="num"><?php echo $show; ?></span><span class="smaller">%</span>
                                            </span>
                                            <div class="pie">
                                                <div class="left-side half-circle"></div>
                                                <div class="right-side half-circle"></div>
                                            </div>
                                            <div class="shadow"></div>
                                        </div>
                                        <div class="row align-items-center pa">
                            </a>
                            <div class="col-12">
                                <h6 class="text-left" style="font-size:20px">
                                    <?php $sql6 = "SELECT customer_name from customer where cus_id = '" . $row_m['customer'] . "' ";
                                    $query6 = mysqli_query($conn, $sql6);
                                    while ($rowc = mysqli_fetch_array($query6, MYSQLI_ASSOC)) { ?>
                                        <?php echo $rowc['customer_name']; ?>
                                    <?php } ?></h6>
                                <h6 class="text-left" style="font-size:20px"><?php echo $row_m["project_name"]; ?></h6>
                                <h6 class="text-left" style="font-size:11px">TentativeCompletion:</h6>
                                <h6 class="text-left" style="font-size:11px"><?php echo $enddate; ?></h6>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php }  ?>
<?php
                $sql5 = "SELECT count(task_id) FROM task where project_id = '" . $project_id . "'  AND customer_request = '1'";
                $query35 = mysqli_query($conn, $sql5);
                $row5 = mysqli_fetch_array($query35, MYSQLI_ASSOC);
                    $num15 = $row5['count(task_id)'];
                
                $sql55 = "SELECT count(task_id) FROM task where project_id = '" . $project_id . "'AND status = '4' AND customer_request = '1'";
                $query355 = mysqli_query($conn, $sql55);
                $row55 = mysqli_fetch_array($query355, MYSQLI_ASSOC);
                    $num155 = $row55['count(task_id)'];
                
                $show5 = @(round($num155 * 100) / $num15);
                if ($show5 == '100') {
                    $query5 = "UPDATE projects SET
                    status = '4',
                    type = '2' 
                    where project_id = '" . $project_id . "'  ";
                    $query9 = mysqli_query($conn, $query5);
                }
?>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="js/simple-donut-jquery.js" type="text/javascript"></script>
<script src="js/jquery.easypiechart.min.js" type="text/javascript"></script>
<script>

    var data = <?php echo $show; ?>;
    // alert(data);
    $("#specificChart<?php echo $n; ?>").show(function() {
        updateDonutChart('#specificChart<?php echo $n; ?>', data, true);
    });
</script>
<?php  } ?>
</div>
</div>
</body>

</html>