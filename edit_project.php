<?php
session_start();
require_once("connect.php");
require_once("session.php");

$project_id = $_GET['project_id'];
$query = "SELECT *  FROM projects where project_id  = '" . $project_id . "' ";
$result = mysqli_query($conn, $query);

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link href="css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="chosen/chosen.css">
    <title>Edit Project</title>
</head>
<body>
<?php   
        include('template/leftbar.php');
        include('template/topbar.php'); 
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){

        
$date =  explode(' ', $row['tentative_completion']);
$start_time  = $date[0];
$datee = explode('-', $start_time);
$year =  $datee[0];
$month = $datee[1];
$day = $datee[2];
        ?>
<div class="container text-left">
    <div class="col-xl-12" style="padding-top: 5rem ;">
        <div class="col-xl-12 text-left">
            <H3>EDIT PROJECT</H3>
            <form action="save_project.php?project_id=<?php echo $row["project_id"]; ?>" method="POST" id="frmsave">
                <div class="form-group form-check"></div>
                    <label>Customer :</label>
                    <?php $sql5 = "SELECT * FROM customer where cus_id = '" . $row['customer'] . "' ";
                    $query5 = mysqli_query($conn, $sql5);
                    while ($row5 = mysqli_fetch_array($query5, MYSQLI_ASSOC)) {   ?>
                        <input value="<?php echo $row5["customer_name"]; ?>" type="text" name="customer" id="customer" readonly class="form-control " placeholder="customer">
                    <?php } ?>
                </div>

                <div class="form-group form-check">
                    <label>Project Name :</label>
                    <input value= "<?php echo $row["project_name"];?>" type="text" name="project_name" id="project_name" class="form-control" placeholder="Project Name">
                    <samp id="show_error" style="color:red;"></samp>
                </div>

                <div class="form-group form-check">
                    <label>Tentative Completion Date:</label>
                    <input value="<?php echo $day . '/' . $month . '/' . $year; ?>" placeholder="Tentative Completion" type="text" id="tentative_completion1" name="tentative_completion1" class="form-control datepicker" autocomplete="off" disabled>
                    <input type="hidden" id="tentative_completion" name="tentative_completion"> 
                </div>

                <div class="form-group form-check">
                    <label>Project Manager :</label>
                    <select class="form-control" id="project_manager" name="project_manager" placeholder="project_manager">
                        <option value="<?php echo $row['project_manager']; ?>"><?php echo $row['project_manager']; ?></option>
                        <?php
                        $sql1 = "SELECT email_address FROM member where   email_address != '" . $row['project_manager'] . "' ";
                        $query1 = mysqli_query($conn, $sql1);
                        while ($row1 = mysqli_fetch_array($query1, MYSQLI_ASSOC)) {
                        ?>
                            <option value="<?php echo $row1["email_address"]; ?>"><?php echo $row1["email_address"]; ?></option>
                        <?php } ?>
                    </select> </div>

                <div class="form-group form-check">
                    <h6>Assigned :</h6>
                    <select data-placeholder="assigned" class="chosen-select1 form-control form-con" multiple tabindex="6" id="assigned" name="assigned[]">
                        <?php
                        $strSql = "SELECT * FROM assigned WHERE  project_id  = '" . $project_id . "' ";
                        $query3 = mysqli_query($conn, $strSql);
                        $num = mysqli_num_rows($query3);
                        $resultArray1 = array();
                        $test = "";
                        for ($i = 0; $i < $num; $i++) {
                            $result3 = mysqli_fetch_array($query3, MYSQLI_ASSOC);
                            $resultArray1[] =   $result3["assigned"];
                        ?>
                            <option selected="selected" value="<?php echo $result3["assigned"]; ?>"><?php echo $result3["assigned"]; ?></option>
                        <?php
                        }
                        $test = implode(',', $resultArray1);
                        $sql3 = "SELECT * FROM member where email_address NOT IN('" . str_replace(",", "','", $test) . "') ";
                        $query4 = mysqli_query($conn, $sql3);
                        while ($row3 = mysqli_fetch_array($query4, MYSQLI_ASSOC)) {
                        ?>
                            <option value="<?php echo $row3["email_address"]; ?>"><?php echo $row3["email_address"]; ?></option>
                        <?php } } ?>
                    </select>
                </div>
       

    <input type="hidden" class="form-control" id="member_id" name="member_id" value="<?php echo $us; ?>">
    <input type="hidden" class="form-control" id="project_id" name="project_id" value="<?php echo $project_id; ?>">          
    <button type="button" class="btn btn-dark btn-rounded" id="addbtn" style="border-radius: 2rem;padding-left: 1.5rem;padding-right: 1.5rem;">Save</button>
    <a href="view_project.php"><button type="button" class="btn btn-dark btn-rounded" style="border-radius: 2rem;padding-left: 1.5rem;padding-right: 1.5rem;">Cancel</button></a>
    </div>
    </div>

</form>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="js/gijgo.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#addbtn').click(function() {
            if ($("#customer").val() == "" && $("#project_name").val() == "" && $("#tentative_completion1").val() == "" && $("#project_manager").val() == "" && $("#assigned").chosen().val() == null) {
                $("#customer").css("border", "1px solid red");
                $("#project_name").css("border", "1px solid red");
                $("#tentative_completion1").css("border", "1px solid red");
                $("#project_manager").css("border", "1px solid red");
                $("#assigned_chosen").css("border", "1px solid red");

                return false;
            }
            if ($("#customer").val() == "") {
                $("#customer").css("border", "1px solid red");
                return false;
            }
            if ($("#project_name").val() == "") {
                $("#project_name").css("border", "1px solid red");
                return false;
            }
            if ($("#tentative_completion1").val() == "") {
                $("#tentative_completion1").css("border", "1px solid red");
                return false;
            }
            if($("#tentative_completion1").val() != "") 
            {
                var te = $("#tentative_completion1").val();
                $("#tentative_completion").val(te);
              //  return false;
            }
            if ($("#project_manager").val() == "") {
                $("#project_manager").css("border", "1px solid red");
                return false;

            }
            if ($("#assigned").chosen().val() == null) {
                $("#assigned_chosen").css("border", "1px solid red");
                return false;
            }
            if ($("#project_name").val() != "") {
                $.ajax({
                    url: "checkproject2.php",
                    data: {
                        project_name: $("#project_name").val(),
                        customer: $("#customer").val(),
                        project_id: $("#project_id").val()

                    },
                    type: "POST",
                    success: function(data) {
                        if (data == 1) {
                            alert(data);
                            $("#show_error").html('มีข้อมูลอยู่แล้ว');
                            $("#project_name").css("border", "1px solid red");
                            return false;
                        }
                        if (data == 0) {
                            $("#frmsave").submit();
                            return false;
                        }

                    }
                });
            }
        });
    });

    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#tentative_completion1').datepicker({
        format: 'dd/mm/yyyy',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today
    });

    $(".chosen-select1").chosen()

    $(function() {
        $("#customer").autocomplete({
            source: 'employee_show.php',
        });
    });
</script>
</body>
</html>