<?php
session_start();
require_once("connect.php");
require_once("session.php");

$task_id = $_GET['task_id'];

$querya = "SELECT * FROM task where task_id  = '" . $task_id . "' ";
$resulta = mysqli_query($conn, $querya);
$rowa = mysqli_fetch_array($resulta, MYSQLI_ASSOC);

$queryb = "SELECT * FROM assigned_task where task_id  = '" . $task_id . "' ";
$resultb = mysqli_query($conn, $queryb);
$rowb = mysqli_fetch_array($resultb, MYSQLI_ASSOC);

$client = $rowa['client'];
$project = $rowa['project'];
$due_date = $rowa['due_date'];
$subject = $rowa['subject'];
$detail = $rowa['detail'];
$upload_file = $rowa['upload_file'];
$datenow = date('Y-m-d'); //วันที่ปัจจุบัน

$date =  explode(' ', $due_date);
$start_time  = $date[0];
$datee = explode('-', $start_time);
$year =  $datee[0];
$month = $datee[1];
$day = $datee[2];

$date1 =  explode(' ', $rowa['assigned_date']);
$start_time1  = $date1[0];
$datee1 = explode('-', $start_time1);
$year1 =  $datee1[0];
$month1 = $datee1[1];
$day1 = $datee1[2];

$sql="SELECT * FROM approved where task_id = '".$task_id."'  ";
$query1 = mysqli_query($conn, $sql);
$num = mysqli_num_rows($query1);

$sql1="SELECT * FROM approved where task_id = '".$task_id."' AND type = '1' ";
$query5 = mysqli_query($conn, $sql1);
$num1 = mysqli_num_rows($query5);

$show = (round($num1*100)/$num);

if(round($show == '100')){
    $query2="SELECT * FROM task where task_id = '".$task_id."'  ";
    $query2 = "UPDATE task SET
    status = '4' 
    where task_id = '".$task_id."'  ";
    $query2 = mysqli_query($conn,$query2);
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <title>Approved</title>
    <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
    <?php include('template/leftbar.php'); ?>
    <?php include('template/topbar.php'); ?>
    <div class="container" style="padding-top: 3rem; padding-left: 6rem; ">
        <h3>APPROVED:</h3>
        <div class="row">
            <div class="col-3">
                <?php
            if ($rowa["status"] == '1' && $rowa['due_date'] >= $datenow) {
                $pathx = "assets/images/power.png";
                $status = '1';
                echo ' <img src="' . $pathx . '" width="150"pix" > ';
            } elseif ($rowa["status"] == '2' && $rowa['due_date'] < $datenow) {
                $pathx = "assets/images/alert1.jpg";
                $status = '2';
                echo '<img src="' . $pathx . '" width="150pix" ;> ';
            } elseif ($rowa["status"] == "3" &&  $rowa['due_date'] < $datenow) {
                $pathx = "assets/images/loading.png";
                echo ' <td class="ng" ><img src="' . $pathx . '" width="150pix"; > </td> ';
            } elseif ($rowa["status"] == "3" && $rowa['due_date'] >= $datenow) {
                $pathx = "assets/images/loading.png";
                echo ' <td><img src="' . $pathx . '" width="150pix"; > </td> ';
            } elseif ($rowa["status"] == "4") {
                $pathx = "assets/images/greencheck.png";
                echo ' <td><img src="' . $pathx . '" width="150pix"; > </td> ';
            }
            elseif ($rowa["status"] == "5") {
                $pathx = "assets/images/notification-bell.png";
                echo '<td><img src="' . $pathx . '" width="150pix"; > </td> ';
                }
            ?>
            </div>
            <div class="col-2" style="padding-top: 1rem ;">Client :
                <br class="col-2">Subject :
                <br class="col-2">AssignedDate :
                <br class="col-2">AssignedBy :
                <br class="col-2">DueDate :
            </div>
            <div class="col-7" style="padding-top: 1rem ;">
                <?php  $sql3 = "SELECT customer_name from customer where cus_id = '".$rowa['client']."' ";
                $query3 = mysqli_query($conn,$sql3);
                while($rowc = mysqli_fetch_array($query3,MYSQLI_ASSOC)){ ?>
                <?php echo $rowc['customer_name']; ?>
                <?php } ?>

                <br class="col-5"><?php echo $rowa['subject']; ?>
                <br class="col-5"><?php echo $day1 . '/' . $month1 . '/' . $year1; ?>
                <br class="col-5">
                <?php $strSql1 = "SELECT * FROM member WHERE  member_id  = '" . $rowa['member_id'] . "'  ";
            $query4 = mysqli_query($conn, $strSql1);
            while($rowf = mysqli_fetch_array($query4,MYSQLI_ASSOC)){ ?>
                <?php echo $rowf["email_address"]; ?>
                <?php } ?>
                <br class="col-5"><?php echo $day . '/' . $month . '/' . $year; ?>
            </div>
        </div>

        <!--------------------------------------------------------------------------------------------------------------------------------------------------->

        <?php
    $query = "SELECT * FROM feedback where task_id  = '" . $_GET['task_id'] . "' ";
    $result = mysqli_query($conn, $query);
    $m = '';
    ?>
        <br>
        <H5>WORKFLOW:</H5>
        <div class="container text-left">
            <div class="col-xl-16">
                <table class="table table-centered mb-0">
                    <tr>
                        <th>NO</th>
                        <th>Workdetail</th>
                        <th>Senddate</th>
                        <th>Sendwork by</th>
                        <th>Final File</th>
                    </tr>
                    <?php
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $m++;
                    $date2 =  explode(' ', $row['feed_date']);
                    $start_time2  = $date2[0];
                    $datee2 = explode('-', $start_time2);
                    $year2 =  $datee2[0];
                    $month2 = $datee2[1];
                    $day2 = $datee2[2];
                ?>
                    <tr>
                        <td><?php echo $m ?></td>
                        <td><?php echo $row['detail']; ?></td>
                        <td><?php echo $day2 . '/' . $month2 . '/' . $year2; ?></td>
                        <td>
                            <?php $query5 = "SELECT email_address FROM member where member_id  = '" . $row['member_id'] . "' ";
                            $result5 = mysqli_query($conn, $query5);
                            $row5 = mysqli_fetch_array($result5, MYSQLI_ASSOC);
                            echo $row5['email_address'];
                            ?>
                        </td>
                        <td>
                            <a href="assets/file/<?php echo $row['upload_file']; ?>" target="_blank">
                                <?php echo $row['upload_file']; ?></a>

                        </td>
                        <td>
                            <img src="assets/images/ellipsis.svg" width="9px" id="addfeed" type="button"
                                data-toggle="modal" data-id="<?php echo $_GET['task_id']; ?>" data-target="#feedmodal">
                        </td>
                    </tr>
                    <?php } ?>
            </div>
        </div>
    </div>

    <tr>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>

        <td class="text-right"><button class="btn btn-dark" id="apmo" type="button" data-toggle="modal"
                data-target="#appmodal" data-id="<?php echo $_GET['task_id']; ?> ">Approved</button></td>
        <td> <a href="follow2.php"><button class="btn btn-dark" type="button">Back</button></a> </td>
    </tr>
    <!--------------------------------------------------------------------------------------------------------------------------------------------------->

    <!--------------------------------------MODAL---------------------------------------------->
    <div class="modal fade" id="appmodal" multiple tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                    <form action="" method="post" id="appadd">
                        <div id="showdata1"> </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="form-control" id="task_id" name="task_id"
                        value="<?php echo $_GET['task_id']; ?>">
                    <button id="inapp" type="button" name="inapp" class="btn btn-dark"
                        data-id="<?php echo $us; ?>">Approved</button>
                    <button type="button" class="btn btn-dark" class="close" data-dismiss="modal"
                        onClick="window.location.reload();">Cancel</button>

                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--------------------------------------MODAL---------------------------------------------->
    <!--------------------------------------MODAL---------------------------------------------->
    <div class="modal fade" id="feedmodal" multiple tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                    <div id="showdata"> </div>
                    <div id="refresh_feed">
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="" method="post" id="feedadd">
                        <input type="text" class="form-control" id="messager" name="messager"
                            placeholder="Feedback Project">
                        <input type="hidden" name="hiddenValue" id="hiddenValue" value="" />
                        <input type="file" class="form-control" id="upload_file" name="upload_file"
                            placeholder="upload_file">
                        <input type="hidden" class="form-control" id="member_id" name="member_id"
                            value="<?php echo $us; ?>">
                        <input type="hidden" class="form-control" id="project_id" name="project_id"
                            value="<?php echo $rowa['project_id']; ?>">
                        <button id="infeed" type="button" name="infeed" class="btn btn-dark"
                            data-id="<?php echo $task_id; ?>">Save</button>
                        <button type="button" class="btn btn-dark" class="close" data-dismiss="modal"
                            onClick="window.location.reload();">Cancel</button>

                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--------------------------------------MODAL---------------------------------------------->


    <!--------------------------------------------------------------------------------------------------------------------------------------------------->
    <script src="js/fileinput.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>

    $(document).on('click', '#addfeed', function() {
        var uid = $(this).attr("data-id");
        var project_id = $("#project_id").val();
        //console.log(uid);
        $("#project_id").val(project_id);
        $.ajax({
            url: "feedshow.php",
            method: "GET",
            data: {
                id: uid,
                project_id: $("#project_id").val()
            },
            success: function(data) {
                $("#feedmodal").modal('show');
                $('#showdata').html(data);
                $(".modal-footer #hiddenValue").val(uid);
            }
        });

        var myVar1 = setInterval(setautoRefresh_div1, 3000);

        function setautoRefresh_div1() {

            var project_id = $('#project_id').val();
            var url = 'feedshow.php?id=' + uid + '&project_id=' + project_id;
            $('#refresh_feed').load(url).show();
        }
    });

    $(document).on('click', '#apmo', function() {
        var uid = $(this).attr("data-id");
        // var status = confirm("Are you Send Alert");
        // if (status) {
        $.ajax({
            url: "approvedshow.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                $("#appmodal").modal('show');
                $('#showdata1').html(data);
            }
        })
        // }
    });

    $(document).on('click', '#inapp', function() {
        var uid = $(this).attr("data-id");
        var task_id = $("#task_id").val()

        // var status = confirm("Are you Send Alert");
        // if (status) {
        $.ajax({
            url: "save_follow.php",
            method: "post",
            data: {
                id: uid,
                task_id: $("#task_id").val()
            },
            success: function(data) {
                location.reload();
            }
        })
        // }
    });


    $(document).ready(function() {
        $('#infeed').click(function(e) {
            if ($("#messager").val() == "") {
                $("#messager").css("border", "1px solid red");
                return false;
            }
            var form_data = new FormData(document.getElementById("feedadd"));
            e.preventDefault();
            $.ajax({
                url: 'feed1.php',
                type: "post",
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                success: function(data) {
                    $("#messager").val('')
                    $("#upload_file").val('')
                }
            });
        });
    });
    </script>
</body>

</html>