<?php
require_once("connect.php");
?>
<!doctype html>
<html lang="en">
<head>
<?php include('template/leftbar.php'); ?>
<?php include('template/topbar.php'); ?>
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="css/bootstrap-datepicker.min.css" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="chosen/docsupport/style.css">
<link rel="stylesheet" href="chosen/docsupport/prism.css">
<link rel="stylesheet" href="chosen/chosen.css">
    <script src="js/chosen.jquery.min.js"></script>
    <script src="js/chosen.jquery.js"></script>
    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <script src="chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>
    <script src="datepicker/dist/js/bootstrap-datepicker-custom.js"></script>
    <script src="datepicker/dist/locales/bootstrap-datepicker.th.min.js"></script>
<!-- jQuery UI -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 
<!-- Bootstrap CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
 
<body>
<div class="container">
  <div class="row">
     <label>CUS:</label>
     <input type="text" name="customer" id="customer" class="form-control">  
  </div>
</div>
 
<script type="text/javascript">
  $(function() {
     $( "#customer" ).autocomplete({
       source: 'employee_show.php',
     });
  });
</script>
 
</body>
</html>