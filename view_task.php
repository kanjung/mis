<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <title>Viewtask</title>
    <link href="css/bootstrap-datepicker.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.min.css">

</head>
<?php
$perpage = 10;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$start = ($page - 1) * $perpage;
$m = 0;
$query = "SELECT * FROM task  GROUP BY task_id DESC  limit {$start},{$perpage} ";
$query_run = mysqli_query($conn, $query);
?>
<body>
<?php include('template/leftbar.php'); ?>
<?php include('template/topbar.php'); ?>
    <div class="container text-left">
        <div class="col-xl-12 text-right" style="padding-top: 5rem ;">
            <a href="task.php" type="button" class="btn btn-dark text-right">AddTask</a>
            <table class="table table-centered mb-0">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>client</th>
                        <th>Name</th>
                        <th>DueDate</th>
                        <th>Subject</th>
                        <th>Detail</th>
                        <th>Assigned</th>
                        <th>Approver</th>
                        <th>UploadFile</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($page > 1) {
                        $m = ($page * $perpage) - 10;
                    }
                    while ($row = mysqli_fetch_assoc($query_run)) {
                        $task_id = $row['task_id'];
                        $m++;

                        $date =  explode(' ', $row['due_date']);
                        $start_time  = $date[0];
                        $datee = explode('-', $start_time);
                        $year =  $datee[0];
                        $month = $datee[1];
                        $day = $datee[2];

                    ?>
                <tbody>
                    <tr>
                        <td> <?php echo $m;  ?> </td>

                        <td> <?php $sql3 = "SELECT customer_name from customer where cus_id = '" . $row['client'] . "' ";
                                $query3 = mysqli_query($conn, $sql3);
                                while ($rowc = mysqli_fetch_array($query3, MYSQLI_ASSOC)) { ?>
                                <?php echo $rowc['customer_name']; ?>
                            <?php } ?> </td>

                        <td> <?php echo $row['project']; ?> </td>
                        <td> <?php echo $day . '/' . $month . '/' . $year;  ?> </td>
                        <td> <?php echo $row['subject']; ?> </td>
                        <td><img src="assets/images/hamburger.jpg" id="viewmo" type="button" width="25px" data-toggle="modal" data-id="<?php echo $row["task_id"]; ?>" data-target="#demodal"></td>

                        <td>
                            <?php
                            $strSql = "SELECT * FROM assigned_task WHERE  task_id  = '" . $row['task_id'] . "'  ";
                            $query3 = mysqli_query($conn, $strSql);
                            $num = mysqli_num_rows($query3);
                            $resultArray = array();

                            for ($i = 0; $i < $num; $i++) {
                                $result3 = mysqli_fetch_array($query3);
                                array_push($resultArray, $result3);
                            } ?>
                            <?php
                            foreach ($resultArray as $result) {
                                echo $result["assigned"];
                            ?>
                            <?php } ?>
                        </td>
                        <td>
                            <?php 
                            $strSql2 = "SELECT * FROM approved WHERE task_id  = '" . $row['task_id'] . "'  ";
                            $query4 = mysqli_query($conn, $strSql2);
                            $num1 = mysqli_num_rows($query4);
                            $resultArray1 = array();
                            for ($i = 0; $i < $num1; $i++) {
                                $result4 = mysqli_fetch_array($query4);
                                array_push($resultArray1, $result4);
                            } ?>
                            <?php
                            foreach ($resultArray1 as $result4) {
                                echo $result4["approved"];
                            ?>
                            <?php } ?>
                        </td>
                        <td> <?php echo $row['upload_file']; ?> </td>
                        <td>
                    <?php 
                    $strSql1 = "SELECT * FROM  assigned_task  WHERE  member_id  = '".$us."' and task_id = '".$task_id."' ";
                    $query3 = mysqli_query($conn, $strSql1);
                    $num = mysqli_num_rows($query3);
                    
                    $strSql11 = "SELECT * FROM  task  WHERE  member_id  = '".$us."' and task_id = '".$task_id."' ";
                    $query31 = mysqli_query($conn, $strSql11);
                    $num1 = mysqli_num_rows($query31);

                    if($num == '1' || $num1 =='1'){
                        echo '<a href="edit_task.php?task_id='.$task_id.'" type="button" class="btn btn-dark" id="edit_data">Edit</a>';
                    }
                    else{
                        echo '<button disabled="disabled" type="button" class="btn btn-dark" id="delete_data">Edit</button>';     
                    }  ?>
                    </td>

                    <td>
                    <?php
                        if ($row['member_id'] == $us) {
                            echo '<button data-id= "' . $task_id . '" type="button" id="delete_data" class="btn btn-dark">Delete</button>';
                        } else {
                            echo '<button disabled="disabled" type="button" class="btn btn-dark" id="delete_data">Delete</button>';
                        }
                            ?>
                        </td>
                    </tr>


                </tbody>

        </div>

    </div>
<?php } ?>

<!--------------------------------------MODAL---------------------------------------------->
<div class="modal fade" id="demodal" multiple tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header"></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">

                <form action="" method="post" id="detail">
                    <div id="showdata"></div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-dark">Back</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--------------------------------------MODAL---------------------------------------------->

<tr>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>

    <td>
    </td>
    <td>
    </td>
    <td>
        <?php
        $sql2 = "SELECT * from task ";
        $query2 = mysqli_query($conn, $sql2);
        $total_record = mysqli_num_rows($query2);
        $total_page = ceil($total_record / $perpage);
        if ($total_record > 10) {
        ?>
            <nav aria-label="Page navigation exaple mt-5">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="view_task.php?page=1"> Previous</a>
                    </li>
                    <?php for ($i = 1; $i <= $total_page; $i++) { ?>
                        <li class="page-item <?php if ($page == $i) {
                                                    echo 'active';
                                                } ?>">
                            <a class="page-link" href="view_task.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li>
                    <?php } ?>
                    <li class="page-item">
                        <a class="page-link" href="view_task.php?page=<?php echo $total_page; ?>">
                            Next
                        </a>
                    </li>
                </ul>
            </nav>
        <?php  }   ?>
    </td>



    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
        <div class="text-right">
            <a href="create.php" type="button" class="btn btn-dark">Back</a>
        </div>
    </td>
</tr>

<!------------------------------------------------------------------>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="js/swal.js"></script>
<script>
    $(document).on('click', '#delete_data', function() {
        var uid = $(this).attr("data-id");
        $.ajax({
            url: "checktask.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                if (data == 1) {
                    swal.fire({
                        icon: 'error',
                        text: 'ไม่สามารถลบข้อมูลได้เนื่องจาก Task ถูกส่งไปแล้ว'
                    })
                }
                if (data == 0) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "ต้องการลบ Task ใช้หรือไม่!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            $.ajax({
                                url: "delete_task.php",
                                method: "post",
                                data: {
                                    id: uid
                                },
                                success: function(data) {
                                    location.reload();
                                }
                            })
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '#viewmo', function() {
        var uid = $(this).attr("data-id");
        // var status = confirm("Are you Send Alert");
        // if (status) {
        $.ajax({
            url: "showdetail.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                $("#demodal").modal('show');
                $('#showdata').html(data);
            }
        })
        // }
    });
</script>
</body>
</html>