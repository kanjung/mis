<!DOCTYPE html>
<html>
<?php
session_start();
require_once("connect.php");
require_once("session.php");
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="chosen/chosen.css">
    <link href="css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <title>Add Task</title>
</head>
<body>
    <?php include('template/leftbar.php'); ?>
    <?php include('template/topbar.php'); ?>
    <form class="form-group" action="add_task.php" method="post" id="frmadd" enctype="multipart/form-data" style="padding-top: 3rem ;">
        <div class="form-row">
            <div class="container text-left">
                <H2>CREATE TASK:</H2>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>Client</label>
                        <select class="client form-control" id="client" name="client" placeholder="client">
                            <option value="">client</option>
                            <?php
                            $sql = "SELECT * FROM customer";
                            $query = mysqli_query($conn, $sql);
                            while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                                $customer = $row["customer_name"];
                            ?>
                                <option value="<?php echo $row['cus_id']; ?>"> <?php echo $customer; ?> </option>
                            <?php } ?>
                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-7">
                        <label>project</label>
                        <div id="projects">
                            <select class=" form-control" id="project">
                                <option value="">project</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-lg-7">
                            <label>subject</label>
                            <input type="text" class="form-control " id="subject" name="subject" placeholder="subject">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-lg-7">
                            <label>Detail</label>
                            <textarea class="form-control" id="detail" name="detail" placeholder="detail" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <div class="form-group ">
                            <label>Assigned</label>
                            <div id="assigned1">
                                <input type="text" class="form-control" placeholder="Assigned"></div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-lg-7">
                            <label>Due Date</label>
                            <input placeholder="Due Date" type="text" id="due_date1" name="due_date1" class="form-control" autocomplete="off" disabled>
                            <input type="hidden" id="due_date" name="due_date"> 
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-lg-7">
                            <label>Upload File</label>
                            <input type="file" class="form-control " id="upload_file" name="upload_file" enctype="multipart/form-data" placeholder="Upload File">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-7">
                            <label>Approved</label>
                            <select data-placeholder="Approved" class="chosen-select2 form-control" multiple tabindex="6" id="approved" name="approved[]">
                                <option value=""></option>
                                <?php
                                $sql1 = "SELECT * FROM member ";
                                $query1 = mysqli_query($conn, $sql1);
                                while ($row1 = mysqli_fetch_array($query1, MYSQLI_ASSOC)) {
                                ?>
                                    <option value="<?php echo $row1["email_address"]; ?>"><?php echo $row1["email_address"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div id="crs"></div>

                    <input type="hidden" class="form-control" id="member_id" name="member_id" value="<?php echo $us; ?>">
                    <button type="button" id="addbtn" class="btn btn-dark" value="upload">Go</button>
                    <a href="view_task.php" type="button" id="addbtn" class="btn btn-dark" value="upload">Cancel</a>

                </div>
            </div>
    </form>
    </div>
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/gijgo.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('#addbtn').click(function() {
                var t = $("#approved").val();

                if ($("#client").val() == "" && $("#project").val() == "" && $("#subject").val() == "" && $("#detail").val() == "" && $("#due_date1").val() == "") {
                    $("#client").css("border", "1px solid red");
                    $("#project").css("border", "1px solid red");
                    $("#subject").css("border", "1px solid red");
                    $("#detail").css("border", "1px solid red");
                    $("#assigned1").css("border", "1px solid red");
                    $("#due_date1").css("border", "1px solid red");
                    $("#approved_chosen").css("border", "1px solid red");
                    return false;
                }


                if ($("#client").val() == "") {
                    $("#client").css("border", "1px solid red");
                    return false;
                }
                if ($("#project").val() == "") {

                    $("#project").css("border", "1px solid red");
                    return false;
                }
                if ($("#subject").val() == "") {

                    $("#subject").css("border", "1px solid red");
                    return false;
                }
                if ($("#detail").val() == "") {

                    $("#detail").css("border", "1px solid red");
                    return false;
                }
                if ($("#assigned").chosen().val() == null) {

                    $("#assigned_chosen").css("border", "1px solid red");
                    return false;
                }
                if ($("#due_date1").val() == "") {

                    $("#due_date1").css("border", "1px solid red");
                    return false;
                }
                if($("#due_date1").val() != "") 
                {
                    var te = $("#due_date1").val();
                    $("#due_date").val(te);
              //  return false;
                }

                if ($("#approved").val() == null) {
                    //  alert(t);
                    $("#approved_chosen").css("border", "1px solid red");
                    return false;
                }
                    if($("#check_error").val() != '0')
                    {
                        if (document.getElementById('cr').checked == false) {
                        
                        $("#error").css("border", "1px solid red");
                        return false;
                        }

                    }
               
                $("#frmadd").submit();
                return false;
            });
        });

        $(".chosen-select2").chosen();

        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#due_date1').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            minDate: today
        });

        $(".client").change(function() {
            var client = $(".client").val();
            $.ajax({
                type: "POST",
                url: "showaddtask.php",
                data: {
                    isclient: client
                },
                success: function(data) {
                    $('#projects').html(data);
                }
            }); 
        });
       


        $("#projects").change(function() {
            var project = $("#project").val();
            $.ajax({
                type: "POST",
                url: "showaddtask1.php",
                data: {
                    isproject: project
                },
                success: function(data) {
                    $('#assigned1').html(data);

                }
            });
            $.ajax({
                    type: "POST",
                    url: "showcr.php",
                    data: {
                        isproject: project
                    },
                    success: function(data) {
                     //   alert(data);
                        $('#crs').html(data);
                    }
            }); 
        });

    </script>
</body>
</html>