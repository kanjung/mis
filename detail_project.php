<!DOCTYPE html>
<html lang="en">
    <?php
    session_start();
    require_once("connect.php");
    require_once("session.php");

    $project_id = $_GET['project_id'];
    $datenow = date('Y-m-d'); //วันที่ปัจจุบัน
    ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/simple-donut.css" type="text/css">
    <link rel="stylesheet" href="chosen/chosen.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Detail Project</title>
    <style>
        .chosen-container {
            width: 465px !important;
        }

        .chosen-container-multi {
            width: 465px !important;
        }
        .pie-wrapper .half-circle1 {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            border: 0.1em solid #c41919;
            border-radius: 50%;
            clip: rect(0em, 0.5em, 1em, 0em);
        }
        body .modal-open {
            padding-right: 0px !important;
            overflow-y: auto;
        }
    </style>
</head>
<body>
    <?php include('template/leftbar.php');
          include('template/topbar.php'); ?>
    <div class="container" style="padding-top: 3rem ; ">
        <h3> MY PROJECT:</h3>
    </div>
    <?php
    $sql_m = "SELECT * FROM projects where project_id = '" . $project_id . "'  ";
    $query_m = mysqli_query($conn, $sql_m);
    $num5 = mysqli_num_rows($query_m);
    if ($num5 < 1) {
        //echo "no data";
    }
    while ($row_m = mysqli_fetch_array($query_m, MYSQLI_ASSOC)) {
        $project_id = $row_m["project_id"];


        $sql = "SELECT count(task_id) FROM task where project_id = '" . $project_id . "'  AND not customer_request = '1' ";
        $query3 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($query3, MYSQLI_ASSOC);
            $num1 = $row['count(task_id)'];
        
        $sql1 = "SELECT count(status) FROM task where project_id = '" . $project_id . "' AND status='4' AND not customer_request = '1'";
        $result1 = mysqli_query($conn, $sql1);
        $row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
            $num = $row1['count(status)'];
        
        if ($num1 != 0) {
            $show = @(round($num * 100) / $num1);
        } else {
            $show = '0';
        }

        $date =  explode(' ', $row_m['tentative_completion']);
        $start_time  = $date[0];
        $datee = explode('-', $start_time);
        $year =  $datee[0];
        $month = $datee[1];
        $day = $datee[2];
        $strdate = $day . '/' . $month . '/' . $year;

    ?>
        <?php if ($row_m['type'] == '1') { ?>
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="text-center" style="padding-top: 1rem; padding-left:1rem; width:250px; height: 250px;">
                            <a href="detail_project.php?project_id=<?php echo $row_m["project_id"]; ?>">
                                <div id="specificChart" class="donut-size">
                                    <div class="pie-wrapper">
                                        <span class="label">
                                            <span class="num"><?php echo $show; ?></span>
                                            <span class="smaller">%</span>
                                        </span>
                                        <div class="pie">
                                            <div class="left-side half-circle1"></div>
                                            <div class="right-side half-circle1"></div>
                                        </div>
                                        <div class="shadow"></div>
                                    </div>
                            </a>
                        </div>
                    </div>
                </div>

            <?php } 
            else { ?>
                <div class="container">
                    <div class="row">
                        <div class="col-3">
                            <div class="text-center" style="padding-top: 1rem; padding-left:1rem; width:250px; height: 250px;">
                                <a href="detail_project.php?project_id=<?php echo $row_m["project_id"]; ?>">
                                    <div id="specificChart" class="donut-size">
                                        <div class="pie-wrapper">
                                            <span class="label">
                                                <span class="num"><?php echo $show; ?></span>
                                                <span class="smaller">%</span>
                                            </span>
                                            <div class="pie">
                                                <div class="left-side half-circle"></div>
                                                <div class="right-side half-circle"></div>
                                            </div>
                                            <div class="shadow"></div>
                                        </div>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php } ?>

                <?php
                $sql5 = "SELECT count(task_id) FROM task where project_id = '" . $project_id . "'  AND customer_request = '1'";
                $query35 = mysqli_query($conn, $sql5);
                while ($row5 = mysqli_fetch_array($query35, MYSQLI_ASSOC)) {
                    $num15 = $row5['count(task_id)'];
                }

                $sql55 = "SELECT count(task_id) FROM task where project_id = '" . $project_id . "'AND status = '4' AND customer_request = '1'";
                $query355 = mysqli_query($conn, $sql55);
                while ($row55 = mysqli_fetch_array($query355, MYSQLI_ASSOC)) {
                    $num155 = $row55['count(task_id)'];
                }

                $show5 = @(round($num155 * 100) / $num15);
                if ($show5 == '100') {
                    $query5 = "SELECT * FROM projects where project_id = '" . $project_id . "'  ";
                    $query5 = "UPDATE projects SET
                status = '4',
                type = '2' 
                where project_id = '" . $project_id . "'  ";
                    $query5 = mysqli_query($conn, $query5);
                }

                ?>

                <br>
                <div class="col-2" style="padding-top: 1rem; font-size:18px">
                    <?php $sql3 = "SELECT customer_name from customer where cus_id = '" . $row_m['customer'] . "' ";
                    $query3 = mysqli_query($conn, $sql3);
                    while ($rowc = mysqli_fetch_array($query3, MYSQLI_ASSOC)) { ?>
                        <?php echo $rowc['customer_name']; ?>
                    <?php } ?>
                    <br class="col-2"> <?php echo $row_m["project_name"]; ?>
                    <br>TentativeCompletionDate:
                    <br><?php echo $strdate; ?>
                    <br>ProjectManager:
                    <br><?php echo $row_m["project_manager"]; ?>
                    <button type="button" class="btn btn-dark btn-rounded" data-toggle="modal" data-target="#bottom-modal" style="margin-top: 8px;
">+Team</button>
                </div>

                </div>
            </div>

            
        <?php  } ?>

        <!--------------------------------------MODAL---------------------------------------------->
        <div class="modal fade" id="bottom-modal" multiple tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form class="form-group" action="add_member.php?project_id=<?php echo $project_id; ?>" method="post" id="frmadd">
                        <div class="modal-header">

                            <h4 class="modal-title" id="myCenterModalLabel">MAKE THE TEAM BIGGER!</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        </div>
                        <div class="modal-body">
                            <select data-placeholder="Add Member" class="chosen-select form-control is-invalid" multiple tabindex="6" id="assigned" name="assigned[]">
                                <?php
                                $strSql = "SELECT * FROM assigned WHERE  project_id  = '" . $project_id . "' ";
                                $query3 = mysqli_query($conn, $strSql);
                                $num = mysqli_num_rows($query3);
                                $resultArray1 = array();
                                $test = "";
                                for ($i = 0; $i < $num; $i++) {
                                    $result3 = mysqli_fetch_array($query3, MYSQLI_ASSOC);
                                    $resultArray1[] =   $result3["assigned"];
                                }
                                $test = implode(',', $resultArray1);

                                $sql5 = "SELECT * FROM member where  email_address NOT IN('" . str_replace(",", "','", $test) . "') ";
                                $query5 = mysqli_query($conn, $sql5);

                                while ($row5 = mysqli_fetch_array($query5, MYSQLI_ASSOC)) {
                                ?>
                                    <option value="<?php echo $row5["email_address"]; ?>"><?php echo $row5["email_address"]; ?>
                                    </option>
                                <?php }  ?>
                            </select>
                        </div>
                        <button type="submit" id="chbtn" class="btn btn-dark " style="margin-left: 10px;" onClick="return check();">Add</button>
                        <button type="button" class="btn btn-dark" class="close" data-dismiss="modal" onClick="window.location.reload();">Cancel</button>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--------------------------------------MODAL---------------------------------------------->

        <div class="container">
            <table class="table table-centered mb-1 rounded-bottom text-right">
                <thead>
                    <?php $query1 = "SELECT DISTINCT assigned FROM task  INNER JOIN 
        assigned_task on task.task_id = assigned_task.task_id where task.project_id= '" . $project_id . "' AND NOT task.subject = '' ";
                    $query_run1 = mysqli_query($conn, $query1);
                    while ($row6 = mysqli_fetch_assoc($query_run1)) {

                    ?>
                </thead>
                <tr>
                    <th><?php echo $row6['assigned']; ?></th>
                </tr>
                <tbody>
                    <?php
                        $query = "SELECT * FROM task  INNER JOIN 
        assigned_task on task.task_id = assigned_task.task_id where task.project_id = '" . $project_id . "' AND task.task_id = assigned_task.task_id AND assigned_task.assigned ='" . $row6['assigned'] . "' 
        AND NOT task.subject = '' ";
                        $query_run = mysqli_query($conn, $query);
                        while ($rowt = mysqli_fetch_assoc($query_run)) {
                            $datenow = date('Y-m-d');  //วันที่ปัจจุบัน
                            $tid =  $rowt['task_id'];

                            $date1 =  explode(' ', $rowt['assigned_date']);
                            $start_time1  = $date1[0];
                            $datee1 = explode('-', $start_time1);
                            $year1 =  $datee1[0];
                            $month1 = $datee1[1];
                            $day1 = $datee1[2];
                            $startdate = $day1 . '/' . $month1 . '/' . $year1;

                            $date2 =  explode(' ', $rowt['due_date']);
                            $start_time2  = $date2[0];
                            $datee2 = explode('-', $start_time2);
                            $year2 =  $datee2[0];
                            $month2 = $datee2[1];
                            $day2 = $datee2[2];
                            $enddate = $day2 . '/' . $month2 . '/' . $year2;

                    ?>
                        <tr>
                        <td>
                        <?php 
                            if($rowt['customer_request'] =='0'){
                             echo   'WA' ;
                            } else{
                             echo 'CR';
                            }
                                
                            ?> 
                            </td>
                            <td> <?php echo $startdate; ?> </td>
                            <td> <?php echo $rowt['subject']; ?></td>
                            <td> <?php echo $enddate; ?></td>
                            <td>
                                <?php
                                if ($rowt["status"] == '1') {
                                    $pathx = "assets/images/power.png";
                                    echo ' <td>On <img src="' . $pathx . '" width="20"pix" ></td>';
                                } elseif ($rowt["status"] == '2') {
                                    $pathx = "assets/images/alert1.jpg";
                                    echo ' <td>Overdue <img src="' . $pathx . '" width="20pix"; > </td> ';
                                } elseif ($rowt["status"] == "3") {
                                    $pathx = "assets/images/loading.png";
                                    echo ' <td>Waiting <img src="' . $pathx . '" width="20pix"; > </td> ';
                                } elseif ($rowt["status"] == "4") {
                                    $pathx = "assets/images/greencheck.png";
                                    echo ' <td>Completed <img src="' . $pathx . '" width="20pix"; > </td> ';
                                } elseif ($rowt["status"] == "5") {
                                    $pathx = "assets/images/notification-bell.png";
                                    echo '<td>Feedback<img src="' . $pathx . '" width="20pix"; > </td> ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $query1 = "SELECT * from notification WHERE  task_id ='" . $rowt['task_id'] . "' AND to_id = $us  ORDER BY no_id DESC LIMIT 1";
                                $run = mysqli_query($conn, $query1);
                                $datafetch = mysqli_fetch_array($run);

                                $query11 = "SELECT * from notification WHERE  task_id ='" . $rowt['task_id'] . "' ORDER BY no_id DESC LIMIT 1";
                                $run1 = mysqli_query($conn, $query11);
                                $datafetch1 = mysqli_fetch_array($run1);

                                $task_id = $rowt['task_id'];
                                
                                if(isset($datafetch['enddate'])!='') {
                                $enddate  = $datafetch['enddate'];
                                }
                                if(isset($datafetch1['enddate'])!=''){
                                $enddate1  = $datafetch1['enddate'];
                                }
                                
                                if ($rowt['status'] == '1' || $rowt['status'] == '4') {
                                    $pathx = "assets/images/time.png";

                                    echo '<button disabled="disabled"><img src="' . $pathx . '" width="30pix";></button>';
                                }

                                if ($rowt['status'] == '2') {
                                    if ($datenow >= $enddate) {
                                        $pathx = "assets/images/time.png";
                                        echo '<button  id="noti" type="button" name="noti" data-id="' . $task_id . '"> <img src="' . $pathx . '" width="30pix" width="30pix"> </button>';
                                    } else {
                                        $pathx = "assets/images/time.png";
                                        echo '<button disabled="disabled"><img src="' . $pathx . '" width="30pix";></button>';
                                    }
                                }

                                if ($rowt['status'] == '3') {
                                    if ($datenow >= $enddate1) {
                                        $pathx = "assets/images/time.png";
                                        echo '<button  id="notiapp" type="button" name="notiapp" data-id="' . $task_id . '"> <img src="' . $pathx . '" width="30pix" width="30pix"> </button>';
                                    } else {

                                        $pathx = "assets/images/time.png";
                                        echo '<button disabled="disabled"><img src="' . $pathx . '" width="30pix";></button>';
                                    }
                                }

                                if ($rowt['status'] == '5') {

                                    if ($datafetch['status'] == '0') {
                                        $pathx = "assets/images/time.png";
                                        echo '<button  id="closer" type="button" name="submit" > <img src="' . $pathx . '" width="30pix" width="30pix"> </button>';
                                    } else {
                                        $pathx = "assets/images/time.png";
                                        echo '<button disabled="disabled"><img src="' . $pathx . '" width="30pix";></button>';
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($rowt['status'] == '4') {
                                    $pathx = "assets/images/ellipsis.svg";
                                    echo '<img src="' . $pathx . '" width="8pix"; ></button>';
                                } else {

                                    echo '<img src="assets/images/ellipsis.svg" id="addfeed" type="button" width="9px" data-toggle="modal" data-id="' . $task_id . '" data-target="#feedmodal">';
                                }
                                ?>
                            </td>

                        </tr>

                </tbody>
        </div>
        </div>
<?php }
                    } ?>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td> <a href="home.php"><button type="button" class="btn btn-dark btn-rounded" style="border-radius: 2rem;padding-left: 1.5rem;padding-right: 1.5rem;">Back</button></a>
   
</tr>

<!--------------------------------------MODAL---------------------------------------------->
<div class="modal fade" id="feedmodal" multiple tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="showdata">
                    <div id="refresh_feed">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <form action="#" method="post" id="feedadd" enctype="multipart/form-data">
                    <input type="text" class="form-control" id="messager" name="messager" placeholder="Feedback Project">
                    <input type="hidden" class="form-control" id="member_id" name="member_id" value="<?php echo $us; ?>">
                    <input type="hidden" class="form-control" id="project_id" name="project_id" value="<?php echo $project_id; ?>">
                    <input type="hidden" name="hiddenValue" id="hiddenValue" value="" />
                    <input type="file" class="form-control" id="upload_file" name="upload_file" placeholder="upload_file">
                    <button id="infeed" type="button" name="infeed" enctype="multipart/form-data" class="btn btn-dark" value="upload">Save</button>
                    <button type="button" class="btn btn-dark" class="close" data-dismiss="modal" onClick="window.location.reload();">Close</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--------------------------------------MODAL---------------------------------------------->
<script src="js/bootstrap.min.js"></script>
<script src="js/simple-donut-jquery.js" type="text/javascript"></script>
<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
<script>
    var data = <?php echo $show; ?>;
    $("#specificChart").show(function() {
        updateDonutChart('#specificChart', data, true);
    });

    $(".chosen-select").chosen();

    $(document).on('click', '#noti', function() {
        var uid = $(this).attr("data-id");

        $.ajax({
            url: "add_noti.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                location.reload();
            }
        })
    });

    $(document).on('click', '#notiapp', function() {
        var uid = $(this).attr("data-id");
        $.ajax({
            url: "add_noti2.php",
            method: "post",
            data: {
                id: uid
            },
            success: function(data) {
                location.reload();
            }
        })
    });

    $(document).on('click', '#addfeed', function() {
        var uid = $(this).attr("data-id");
        var project_id = $("#project_id").val();
        //console.log(uid);
        $("#project_id").val(project_id);
        $.ajax({
            url: "feedshow.php",
            method: "GET",
            data: {
                id: uid,
                project_id: $("#project_id").val()
            },
            success: function(data) {
                $("#feedmodal").modal('show');
                $('#showdata').html(data);
                $(".modal-footer #hiddenValue").val(uid);
            }
        });

        var myVar1 = setInterval(setautoRefresh_div1, 3000);
        function setautoRefresh_div1() {
            var project_id = $('#project_id').val();
            var url = 'feedshow.php?id=' + uid + '&project_id=' + project_id;
            $('#refresh_feed').load(url).show();
        }
    });

    $(document).ready(function() {
        $('#infeed').click(function(e) {
            if ($("#messager").val() == "") {
                $("#messager").css("border", "1px solid red");
                return false;
            }
            var form_data = new FormData(document.getElementById("feedadd"));
            e.preventDefault();
            $.ajax({
                url: 'feed.php',
                type: "post",
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                success: function(data) {
                    $("#messager").val('')
                    $("#upload_file").val('')
                }
            });
        });
    });

    $(document).ready(function() {
        $('#chbtn').click(function() {

            if ($("#assigned").val() == null) {
                $("#assigned_chosen").css("border", "1px solid red");
                return false;
            }
        })
    });

</script>
</body>
</html>