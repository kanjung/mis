<?php
	session_start();
    require_once("connect.php");

	unset ($_SESSION["member_id"]);
	unset ($_SESSION["email_address"]);
	session_destroy();
    header("location:index.php");
?>